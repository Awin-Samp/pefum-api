const { response } = require('express');
const mysql = require('mysql');
const db = require('./database.js')


class corporate_db{
    constructor(){
        global.db = db;
    }
     // CORPORATE DATABASE SECTION--------------------------------------------------------------
    // registration for CORPORATES
    register_account(corp_id, name, logo, login_name, password, phone, email, address, active, date, time , callback){
        let query = "INSERT INTO `corporates` (corp_id, name, logo, login_name, password, phone, email, address, active, date, time) VALUES (?)";
        var values = [corp_id, name, logo, login_name, password, phone, email, address, active, date, time];
        
        try {
            db.query(query, [values], (err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                console.log(response);
                return callback({
                    status:true,
                    message:'Registration successful',
                    response});
    
            })
        } catch (error) {
            console.log('MyError', error)
            return callback({
                status:false,
                message:'Failed Registration',
                });
        }
    }

     // LOGIN SECTION ---------------------------------------------------------------------------
     get_corporate(login_name , callback ){
        let query = "SELECT * FROM corporates WHERE login_name=" + login_name;
        
            if(login_name.length != 0){
                try {
                    db.query(query, (err, response)=>{
                        if(err) throw err ; 
                        if(response.length == 0){
                            console.log('response', response[0])
                            return callback({
                                status: false,
                                message:'response is null'
                            });
                        } 
                            return callback({
                                status: true,
                                response: response[0]});  
                            })
                } catch (error) {
                    return callback({
                        status: false,
                        msg:'Login failed (login name error)'});
                }
            }
            else{  
                return callback({
                status: false,
                msg:'login failed (login name error)'});
            }
    }

    // CREATE NEW BRANCH-----------------------------------------------------------------------------
    create_branch(corp_branch_id, corp_id, name, logo, login_name, password, wallet, phone, email, address, active, date, time ,callback){
        let query = "INSERT INTO `corporates` (corp_branch_id, corp_id, name, logo, login_name, password, wallet, phone, email, address, active, date, time) VALUES (?)";
        var values =[corp_branch_id, corp_id, name, logo, login_name, password, wallet, phone, email, address, active, date, time]
        try {
            db.query(query, [values], (err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                return callback({
                    status:true,
                    message:'Branch Registration successful',
                    response});
    
            })
        } catch (error) {
            console.log('MyError', error)
            return callback({
                status:false,
                message:'Failed Branch Registration',
                });
        }
  
    }
    // GET ALL BRANCHES
    get_all_branches(corp_id , callback){
        let query = 'SELECT * FROM corporate_branches WHERE corp_id='+corp_id;
        try{
            db.query(query,(err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                return callback({
                    status:true,
                    message:'All branches requested successfully',
                    response});
            })
        } catch (error) {
            console.log('MyError', error)
            return callback({
                status:false,
                message:'Failed to request for all branches',
                });
        }

    }

    // GET SINGLE BRANCH
    get_branch(corp_branch_id , callback){
        let query = 'SELECT * FROM corporate_branches WHERE corp_branch_id='+corp_branch_id;
        try{
            db.query(query,(err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                return callback({
                    status:true,
                    message:'Corporate Branch requested successfully',
                    response:response[0]});
            })
        } catch (error) {
            console.log('MyError', error)
            return callback({
                status:false,
                message:'Failed to request for corporate branch',
                });
        }

    }

    
    // DELETE SINGLE BRANCH
    delete_branch(corp_branch_id , callback){
        let query = 'UPDATE corporate_branches SET active=3 WHERE corp_branch_id='+corp_branch_id;
        try{
            db.query(query,(err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    returncallback({
                        status: false,
                        message:'response is null'
                    });
                }
                return callback({
                    status:true,
                    message:'Corporate Branch Deleted successfully',
                    response});
            })
        } catch (error) {
            console.log('MyError', error)
            return callback({
                status:false,
                message:'Failed to delete for corporate branch',
                });
        }

    }

    
    // ACTIVATE OR DEACTIVATE BRANCH--- gets current state and toggle it to true or false
    activate_deactivate_branch(corp_branch_id , callback){
        let query = 'SELECT active FROM corporate_branches WHERE corp_branch_id='+corp_branch_id;
        try{
            // Gets current active state of branch
            db.query(query,(err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                if(response!=null){
                    var state = response[0].active;
                    // toggles the current active state
                    let state_query = 'UPDATE corporate_branches SET active='+ !state+ ' WHERE corp_branch_id='+corp_branch_id;
                    db.query(state_query, (err, result)=>{
                        if(err) throw err ; 
                        if(response.length == 0){
                            console.log('response', response[0])
                            return callback({
                                status: false,
                                message:'response is null'
                            });
                        }
                        return callback({
                            status:true,
                            message:'Corporate Branch active-state TOGGLED successful',
                            });
                    })
                }
            })
        } catch (error) {
            console.log('MyError', error)
            return callback({
                status:false,
                message:'Failed to TOOGLE active-state for corporate branch',
                });
        } 
    }


    
    // TRANSACTIONS SECTION (main corp)------------------------------------------------------------
    get_transactions(corp_id , callback){
        let query = 
        "SELECT * FROM `coupon_transactions` \
        WHERE corp_id=" + corp_id + " ORDER BY my_time" ;
        var TRANSACTION= [];
        try {
            db.query(query, (err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                // return callback(response)
                for(var i=0; i<response.length; i++ ){
                    // gets basic employee information
                    this.get_employee_info(response[i].coupon_code, (result)=>{
                        if(result!=null){
                            // gets the location the fuel was bought
                            this.get_fuel_location(response[i].station_branch_id,(resp)=>{
                                if(resp!=null){
                                    var obj= {
                                        'transaction_id':response[i].transaction_id,
                                        'pefum_code':response[i].pefum_code,
                                        'attendant_code':response[i].attendant_code,
                                        'fuel_station_id':response[i].fuel_station_id,
                                        'station_branch_id':response[i].station_branch_id,
                                        'amount':response[i].amount,
                                        'fuel_type':response[i].fuel_type,
                                        'status':response[i].status,
                                        'date':response[i].my_date,
                                        'time':response[i].my_time,
                                        // data from employee table 
                                        'coupon_code':result.coupon_code,
                                        'first_name':result.first_name,
                                        'last_name':result.last_name,
                                        'corp_branch':result.corp_branch,
                                        'top_up':result.top_up,
                                        'wallet':result.wallet,
                                        // get location
                                        'location':resp.location
                                    }
                                    //add custom transaction to transaction ARRAY
                                    TRANSACTION.push(obj);
                                }else{
                                    return callback({
                                        status:false,
                                        message:'failed to get all transactions(bcus of fuel_location)'
                                    })
                                }
                            })
                        }
                        else{
                            return callback({
                                status:false,
                                message:'failed to get all transactions(bcus of employee)'
                            })
                        }
                    })
                }
                // send the new Transaction array as response
                return callback({
                    status:true,
                    message:'Transactions requested successful',
                    response:TRANSACTION});
            })
        } catch (error) {
            console.log('MyError', error)
            return callback({
                status:false,
                message:'failed to get all transactions'
            })
        }
    }


    // TRANSACTIONS SECTION (branch corp)------------------------------------------------------------
    get_branch_transactions(corp_branch_id , callback){
        let query = 
        "SELECT * FROM `coupon_transactions` \
        WHERE corp_branch_id=" + corp_branch_id + " ORDER BY my_time" ;
        var TRANSACTION= [];
        try {
            db.query(query, (err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                for(var i=0; i<response.length; i++ ){
                    // gets basic employee information
                    this.get_employee_info(response[i].coupon_code, (result)=>{
                        if(result!=null){
                            // gets the location the fuel was bought
                            this.get_fuel_location(response[i].station_branch_id,(resp)=>{
                                if(resp!=null){
                                    var obj= {
                                        'transaction_id':response[i].transaction_id,
                                        'pefum_code':response[i].pefum_code,
                                        'attendant_code':response[i].attendant_code,
                                        'fuel_station_id':response[i].fuel_station_id,
                                        'station_branch_id':response[i].station_branch_id,
                                        'amount':response[i].amount,
                                        'fuel_type':response[i].fuel_type,
                                        'status':response[i].status,
                                        'date':response[i].my_date,
                                        'time':response[i].my_time,
                                        // data from employee table 
                                        'coupon_code':result.coupon_code,
                                        'first_name':result.first_name,
                                        'last_name':result.last_name,
                                        'corp_branch':result.corp_branch,
                                        'top_up':result.top_up,
                                        'wallet':result.wallet,
                                        // get location
                                        'location':resp.location
                                    }
                                    //add custom transaction to transaction ARRAY
                                    TRANSACTION.push(obj);
                                }else{
                                    return callback({
                                        status:false,
                                        message:'failed to get all transactions(bcus of fuel_location)'
                                    })
                                }
                            })
                        }
                        else{
                            return callback({
                                status:false,
                                message:'failed to get all transactions(bcus of employee)'
                            })
                        }
                    })
                }
                // send the new Transaction array as response
                return callback({
                    status:true,
                    message:'Transactions requested successful',
                    response:TRANSACTION});
            })
        } catch (error) {
            console.log('MyError', error)
            return callback({
                status:false,
                message:'failed to get all transactions'
            })
        }
    }

    

    // EMPLOYEE TRANSACTIONS SECTION (coupon transaction)--------------------------------------------------
    get_employee_transaction(coupon_code , callback){
        let query = 
        "SELECT * FROM `coupon_transactions` \
        WHERE coupon_code=" + coupon_code + " ORDER BY my_time" ;
        var TRANSACTION= [];
        // first get the employees data
        this.get_employee_info(coupon_code,(response)=>{
            if(response==null){
                return callback({
                    status:false,
                    message:'failed to get all transactions'
                })
                return;
            }
             var employee_obj = {
                'first_name':response.first_name,
                'last_name':response.last_name,
                'corp_branch':response.corp_branch,
                'pefum_code':response.pefum_code,
                'coupon_code':response.coupon_code,
                'top_up':response.top_up,
                'wallet':response.wallet,
             }
            
            //second get all the transaction for this employee
            try {
                db.query(query,(err, result)=>{
                    if(err) throw err ; 
                    if(response.length == 0){
                        console.log('response', response[0])
                        return callback({
                            status: false,
                            message:'response is null'
                        });
                    }
                    if(result!=null){ 
                        for(var i=0; i<result.length; i++){
                        //third get the location for each transaction
                            this.get_fuel_location(result[i].station_branch_id, (resp)=>{
                                var location = resp.location;
                                var fuel_station = resp.company_name;

                                //organize all the data into an object
                                var transaction_obj ={
                                        'transaction_id':result[i].transaction_id,
                                        'attendant_code':result[i].attendant_code,
                                        'fuel_station_id':result[i].fuel_station_id,
                                        'station_branch_id':result.station_branch_id,
                                        'amount':result[i].amount,
                                        'fuel_type':result[i].fuel_type,
                                        'status':result[i].status,
                                        'date':result[i].my_date,
                                        'time':result[i].my_time,
                                        // get location and fuel station name
                                        'location':location,
                                        'fuel_station':fuel_station
                                }
                            })
                            TRANSACTION.push(transaction_obj);
                        }
                        // return employee data + all transactions
                        return callback({
                            status:true,
                            message:'All employee transactions requested',
                            response:{
                                employee:employee_obj,
                                data:TRANSACTION 
                            }
                        })

                    }
                    else{
                        return callback({
                            status:false,
                            message:'failed to get all transactions'
                        })
                    }
                })
            } catch (error) {
                return callback({
                    status:false,
                    message:'failed to get all transactions'
                })
            }
        })
    }


    
    //SINGLE TRANSACTIONS SECTION (coupon transaction)------------------------------------------------------------
    get_single_transaction(transaction_id , callback){
        let query = 
        "SELECT * FROM `coupon_transactions` \
        WHERE transaction_id=" + transaction_id ;
        try {
            db.query(query, (err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                if(response!=null){
                    // gets basic employee information
                    this.get_employee_info(response[0].coupon_code, (result)=>{
                        if(result!=null){
                            // gets the location the fuel was bought
                            this.get_fuel_location(response[0].station_branch_id,(resp)=>{
                                if(resp!=null){
                                    var obj= {
                                        'transaction_id':response[i].transaction_id,
                                        'pefum_code':response[i].pefum_code,
                                        'attendant_code':response[i].attendant_code,
                                        'fuel_station_id':response[i].fuel_station_id,
                                        'station_branch_id':response[i].station_branch_id,
                                        'amount':response[i].amount,
                                        'fuel_type':response[i].fuel_type,
                                        'status':response[i].status,
                                        'date':response[i].my_date,
                                        'time':response[i].my_time,
                                        // data from employee table 
                                        'coupon_code':result.coupon_code,
                                        'first_name':result.first_name,
                                        'last_name':result.last_name,
                                        'corp_branch':result.corp_branch,
                                        'top_up':result.top_up,
                                        'wallet':result.wallet,
                                        // get location
                                        'location':resp.location
                                    }
                                    return callback({
                                        status:true,
                                        message:"Single Transaction requested successful",
                                        response:obj
                                    })
                                }else{
                                    return callback({
                                        status:false,
                                        message:'failed to get single transaction'
                                    })
                                }
                            })
                        }
                        else{
                            return callback({
                                status:false,
                                message:'failed to get single transaction'
                            })
                        }
                    })
                }
            })
        } catch (error) {
            console.log('MyError', error)
            return callback({
                status:false,
                message:'failed to get single transaction'
            })
        }
    }


      //get employee basic info  
      get_employee_info(coupon_code , callback){
        let query = 'SELECT * FROM employees WHERE coupon_code=' + coupon_code;
        try {
            db.query(query, (err,response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                return callback(response[0])
            })
        } catch (error) {
            console.log('MyError', error)
        }
    }
     //get location fuel was bought  
     get_fuel_location(station_branch_id , callback){
        let query = 'SELECT location,company_name FROM station_branches WHERE station_branch_id=' + station_branch_id;
        try {
            db.query(query, (err,response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                return callback(response[0])
            })
        } catch (error) {
            console.log('MyError', error)
        }
    }
    // -------------------------------------------------------------------------------------------------------


    // ADD NEW EMPLOYEE
    add_employee(pefum_code,coupon_code,first_name,last_name,phone,corp_id,corp_branch_id,corp_name,corp_branch,top_up,wallet,fuel_station_subscribed,active,date,time , callback ){
        let query = 'INSERT INTO employees (pefum_code,coupon_code,first_name,last_name,phone,corp_id,corp_branch_id,corp_name,corp_branch,top_up,wallet,fuel_station_subscribed,active,date,time) VALUES (?)';
        var values = [pefum_code,coupon_code,first_name,last_name,phone,corp_id,corp_branch_id,corp_name,corp_branch,top_up,wallet,fuel_station_subscribed,active,date,time];
        
        //checks if pefum_code is in users db before adding new employee
        this.check_pefum_code(pefum_code,(result)=>{
            if(result.status==true){
                try {
                    //adds new employee
                    db.query(query,[values], (err, response)=>{
                        if(err) throw err ; 
                        if(response.length == 0){
                            console.log('response', response[0])
                            return callback({
                                status: false,
                                message:'response is null'
                            });
                        }
                        return callback({
                            status: true,
                            message: 'Employee added successfully',
                            other:result.message
                        })
                    })
                } catch (error) {
                    return callback({
                        status: false,
                        message: 'failed to add employee'
                    })
                }
            }else{
                return callback({
                    status: false,
                    message: result.message
                })
            }
        })
    }

    // check if pefum_code is in usersdb before employee is added
    check_pefum_code(pefum_code, callback ){
        let query = 'SELECT * FROM users WHERE pefum_code='+pefum_code;
        try {
            db.query(query, (err,response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                return callback({
                    status:true,
                    message:'Pefum code accurate (Confirmed PFM app User)',
                    response:response[0]
                })
            })
        } catch (error) {
            return callback({
                status:false,
                message: 'Pefum code does not exist (Must register on PFM app first)'
            })
            
        }
    }

    //after adding employee, update usersdb coupon code and wallet-----------------------------------------
    update_user_coupon_n_wallet(pefum_code, coupon_code,wallet, callback ){
        let query = 'UPDATE users SET coupon_code='+coupon_code+ ',coupon_wallet='+wallet +' WHERE pefum_code='+pefum_code;
        try {
            db.query(query,(err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                return callback({
                    status:true,
                    message: 'coupon code: '+coupon_code +'and wallet GHC'+wallet+' added to user with pefum code: ' +pefum_code
                })
            })
        } catch (error) {
            return callback({
                status:false,
                message:'Error adding coupon code to user with pefum code: '+ pefum_code
            })
        }
    }


      //UPDATE EMPLOYEE AND USERS COUPON WALLET-----------------------------------------
      update_user_n_employee_wallet(employee_id,top_up, callback ){
        let query = 'SELECT * FROM employees WHERE employee_id='+employee_id;
        try {
            //get coupon wallet
            db.query(query,(err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                    // check if topup is not 0 (zero)
                    if(top_up>0){
                        //do the additions
                        var new_balance = parseFloat(response[0].wallet) + top_up;
                        let user_query = 'UPDATE users SET coupon_wallet='+new_balance+' WHERE pefum_code='+ response[0].pefum_code;
                        try { //updates the users coupon wallet
                            db.query(user_query,(err, result)=>{
                                if(err) throw err ; 
                                if(response.length == 0){
                                    console.log('response', response[0])
                                    return callback({
                                        status: false,
                                        message:'response is null'
                                    });
                                }
                                let emp_query = 'UPDATE employees SET wallet='+new_balance+' WHERE employee_id='+employee_id;
                                try {//update employees coupon wallet
                                    db.query(emp_query,(error, result)=>{
                                        if(err) throw err ; 
                                        if(response.length == 0){
                                            console.log('response', response[0])
                                            return callback({
                                                status: false,
                                                message:'response is null'
                                            });
                                        }
                                        return callback({
                                            status:true,
                                            message: 'coupon wallet updated to GHC'+new_balance
                                        })
                                    })
                                } catch (error) {
                                    return callback({
                                        status:false,
                                        message:'failed to update employees coupon_wallet'
                                    })
                                }
                            })
                        } catch (error) {
                            return callback({
                                status:false,
                                message:'failed to update users coupon_wallet'
                            })
                        }
                    }else{
                        return callback({
                            status:false,
                            message:'topup must be greater than zero or != NaN'
                        })
                    }
            })
        } catch (error) {
            return callback({
                status:false,
                message:'Error get current coupon wallet balance',
                other:'Error updating coupon wallet '
            })
        }
    }


    // GET ALL EMPLOYEES
    get_employees(corp_id , callback){
        let query = 'SELECT * FROM employees WHERE corp_id='+corp_id
        try {
            db.query(query, (err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                return callback({
                    status: true,
                    message: "Employee request successful",
                    response
                })
            })
        } catch (error) {
            return callback({
                status: false,
                message: 'failed to get employees'
            })
        }
    }

     // GET ALL EMPLOYEES (branch only)
     get_branch_employees(corp_branch_id , callback){
        let query = 'SELECT * FROM employees WHERE corp_branch_id='+corp_branch_id
        try {
            db.query(query, (err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                return callback({
                    status: true,
                    message: "Employees request successful(branch only)",
                    response
                })
            })
        } catch (error) {
            return callback({
                status: false,
                message: 'failed to get employees(branch only)'
            })
        }
    }


     // GET SINGLE EMPLOYEE
     get_single_employee(employee_id , callback){
        let query = 'SELECT * FROM employees WHERE employee_id='+employee_id
        try {
            db.query(query, (err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                return callback({
                    status: true,
                    message: "Employee request successful",
                    response:response[0]
                })
            })
        } catch (error) {
            return callback({
                status: false,
                message: 'failed to get employee'
            })
        }
    }

    
    
     // DELETE SINGLE EMPLOYEE
     delete_employee(employee_id , callback){
        let query = 'UPDATE employees SET active=3 WHERE employee_id='+employee_id
        try {
            db.query(query, (err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                return callback({
                    status: true,
                    message: "Employee deleted successful",
                    response:response[0]
                })
            })
        } catch (error) {
            return callback({
                status: false,
                message: 'failed to delete employee'
            })
        }
    }


     // ACTIVATE OR DEACTIVATE EMPLOYEE--- gets current state and toggle it to true or false
     activate_deactivate_employee(employee_id , callback){
        let query = 'SELECT active FROM employees WHERE employee_id='+employee_id;
        try{
            // Gets current active state of branch
            db.query(query,(err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                if(response!=null){
                    var state = response[0].active;
                    // toggles the current active state
                    let state_query = 'UPDATE employees SET active='+ !state+ ' WHERE employee_id='+employee_id;
                    db.query(state_query, (err, result)=>{
                        if(err) throw err ; 
                        if(response.length == 0){
                            console.log('response', response[0])
                            return callback({
                                status: false,
                                message:'response is null'
                            });
                        }
                        return callback({
                            status:true,
                            message:'employee active-state TOGGLED successful',
                            });
                    })
                }
            })
        } catch (error) {
            console.log('MyError', error)
            return callback({
                status:false,
                message:'Failed to TOOGLE active-state for employee',
                });
        } 
    }


    // CHECK CORPORATE BALANCE ,THEN UPDATE BALANCE---------------------------------------------
    //so use this to check balance before adding a new employee(with top-up field != 0)
    //and then update the wallet to new balance after deductions
    check_and_update_corp_wallet(corp_id,employee_top_up_amount, callback ){
        let qeury = 'SELECT wallet FROM corporates WHERE corp_id='+corp_id;
        try {
            //gets the wallet amount of corporate
            db.query(qeury, (err, response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                if(employee_top_up_amount>0){//checks if top-up amount entered is not zero
                    var corp_wallet = parseFloat(response[0].wallet);
                    if(corp_wallet>=employee_top_up_amount){//checks if there's enough ballance
                        //now we can update the corp wallet
                        var new_balance = corp_wallet - parseFloat(employee_top_up_amount);
                        let update_query = 'UPDATE corporates SET wallet='+new_balance+' WHERE corp_id='+corp_id;
                        try {
                            db.query(update_query,(err, result)=>{
                                if(err) throw err ; 
                                if(response.length == 0){
                                    console.log('response', response[0])
                                    return callback({
                                        status: false,
                                        message:'response is null'
                                    });
                                }
                                return callback({
                                    status:true,
                                    message:'Checked and Updated corporate wallet',
                                    other:'You can now add employee'
                                })
                            })
                        } catch (error) {
                            return callback({
                                status:false,
                                message:'failed to check and update corporate wallet',
                                other:'You don`t have permission to add employee'
                            })
                        }
                    }
                }else if(employee_top_up_amount==0){
                    return callback({
                        status:true,
                        message:'top-up amount is 0 (zero)'
                    })
                }else{
                    return callback({
                        status:false,
                        message:'error from top-up: '+employee_top_up_amount
                    })
                }
            })
        } catch (error) {
            return callback({
                status:false,
                message:'error requesting for corporate wallet'
            })
        }
    }

    //UPDATE BRANCH WALLET BALANCE
    update_branch_wallet(corp_branch_id,top_up, callback ){
        let query = 'SELECT wallet FROM corporate_branches WHERE corp_branch_id='+corp_branch_id;
        //get branch wallet
        try {
            db.query(query,(err,response)=>{
                if(err) throw err ; 
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                //make additions
                var new_balance = parseFloat(response[0].wallet) + top_up;
                //update branch wallet with new balance
                let wquery = 'UPDATE corporate_branches SET wallet='+new_balance+' WHERE corp_branch_id='+corp_branch_id;
                try {
                    db.query(wquery, (error, result)=>{
                        if(err) throw err ; 
                        if(response.length == 0){
                            console.log('response', response[0])
                            return callback({
                                status: false,
                                message:'response is null'
                            });
                        }
                        return callback({
                            status:true,
                            message:'Corporate Branch update successfull with GHC '+ new_balance
                        })
                    })
                } catch (error) {
                    return callback({
                        status:false,
                        message:'Failed to top up corp branch'
                    })
                }
            })
        } catch (error) {
            return callback({
                status:false,
                message:'Failed to get corp branch wallet balance',
                other:'Failed to top up corp branch'
            })
        }
    }

    
}

module.exports = corporate_db;