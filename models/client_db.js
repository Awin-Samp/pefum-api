const { response } = require('express');
const mysql = require('mysql');
const db = require('./database.js')


class client_db{
    constructor(){
        global.db = db;
    }

    // USERS DATABASE SECTION--------------------------------------------------------------
    // registration for users to userdb
    register_user(first_name,last_name,email,phone,password,pefum_code,otp_code,active,date,time, callback){
        let query = "INSERT INTO `users` (`first_name`, `last_name`, `email`, `phone`, `password`, `pefum_code`, `otp_code`, `active`, `p_verified`,`date`,`time`) VALUES (?)";
        var values = [first_name, last_name, email, phone, password, pefum_code, otp_code, active,false,date,time];
        
        // check if phone number is not already registered
        this.get_user(phone,(result)=>{
            if(result.status!=true){//means phone is not registered, so can add new user
                try {
                    db.query(query, [values], (err, response)=>{
                        if(err){ return callback({status: false,message: 'error here',}); }  
                        console.log('DATA', response)
                        if(response.length==0){
                            return callback(
                                {status:false,
                                 message:'No data available',
                                });
                        }
                        return callback({
                            status:true,
                            message:'user registered successfully',
                            response});
            
                    })
                } catch (err) {
                    console.log('Myerr', err)
                    return callback({
                        status:false,
                        message:'failed user registration',
                        });
                }
            }else{
                console.log('get user phone exist', '')
                return callback({
                    status: false,
                    message: 'Phone number already registered'
                })
            }
        })
    }
    // PHONE VERIFIED by SMS
    phone_verified(pefum_code, callback){
        let query = 'UPDATE users SET p_verified=true WHERE pefum_code='+pefum_code;
        db.query(query,(err, response)=>{
            if(err){ return callback({status: false,message: 'error here',}); }  
            if(response.length==0){
                return callback(
                    {status:false,
                     message:'Wrong pefum code(token)',
                    });
            }
            // if(response.changedRows==0){
            //     return callback({
            //         status: false,
            //         message:'p-verified not updated(maybe already p-verified=1)'
            //     });
            // }
            console.info('changedrows', response.changedRows)
            return callback({
                status:true,
                message:'phone verified successfully'
            })
        })
    }


     // check otp in database
     check_otp_by_pefum(pefum_code, otp, callback){
        let query = 'SELECT * FROM users WHERE pefum_code="' +pefum_code+'" AND otp_code='+otp;
        try {
            db.query(query, (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                console.log('DATA', response)
                if(response.length==0){
                    return callback(
                        {status:false,
                         message:'failed Otp confirmation',
                        });
                }else{
                    //update user p-verified
                    let up_qeury = "UPDATE users SET p_verified=1 WHERE pefum_code="+pefum_code;
                    try {
                        db.query(up_qeury, (err, result)=>{
                            if(err){ return callback({status: false,message: 'error here',}); }  
                            console.log('DATA', response)
                            if(result.length==0){
                                return callback(
                                    {status:false,
                                    message:'failed to update p_verified',
                                    });
                            }else{
                                return callback(
                                    {status:true,
                                     message:'Otp confirmed',
                                    response});
                            }
                        })
                    } catch (error) {
                        return callback({
                            status:false,
                             message:'failed to update',
                            });
                    }
                }
            })
        } catch (err) {
            console.log('Myerr', err)
            return callback(
                {status:false,
                 message:'failed Otp confirmation',
                });
        }
    }

       // get otp in database
       get_otp_by_phone(phone,callback){
        let query = 'SELECT otp_code FROM users WHERE phone=' +phone;
        try {
            db.query(query, (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(response.length==0){
                    return callback(
                        {status:false,
                         message:'Wrong phone',
                        });

                }
                console.log('DATA', response)
                return callback(
                    {status:true,
                     message:'Otp request confirmed',
                    response: response[0]});
            })
        } catch (err) {
            console.log('Myerr', err)
            return callback(
                {status:false,
                 message:'failed Otp request',
                });
        }
    }

    // LOGIN SECTION ---------------------------------------------------------------------------
    get_user(phone, callback){
        let query = "SELECT * FROM users WHERE phone=" + phone;
        
            if(!Number.isNaN(phone)){
               try {
                db.query(query, (err, response)=>{
                    if(err){ return callback({status: false,message: 'error here',}); }  
                    if(response.length == 0){ 
                        return callback({
                            status: false,
                            message:'response is null'
                        }); 
                    }else{
                        console.log('user response', response[0])
                        return callback({
                            status: true,
                            message: 'Phone number is found',
                            response:response[0]});
                    }
                })
               } catch (err) {
                    return callback({
                        status: false,
                        message:'failed user login (phone not in database)'
                    });
               }
            }
            else{
                return callback({
                    status: false,
                    msg:'failed user login (phone NaN err)'});
            }   
    }

    // FORGOT PASSWORD SECTION -------------------------------------------------------------
    // renew user otp
    change_otp(phone, new_otp, callback){
        let query = "UPDATE users SET otp_code=" +new_otp+ " WHERE phone=" +phone;
        try {
            db.query(query, (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                    if(response.length == 0){
                        console.log('response', response[0])
                        return callback({
                            status: false,
                            message:'response is null'
                        });
                    } 
                    if(response.changedRows==0){
                        return callback({
                            status: false,
                            message:'response is null'
                        });
                    }
                return callback({
                    status: true,
                    message: 'OTP renewal successful',
                    response:new_otp})//user callback to send otp sms
            })
        } catch (err) {
            console.log('Myerr', err)
            return callback({
                status: false,
                message: 'failed OTP renewal'})
        }
    }

    // check otp in database
    check_otp(phone, otp, callback){
        let query = 'SELECT * FROM users WHERE phone=' +phone+' AND otp_code='+otp;
        try {
            db.query(query, (err, response)=>{
                if(err){
                    console.log('Myerr', err)
                    return callback({
                        status: false,
                        message: 'error here',});
                } 
                    if(response.length == 0){
                        console.log('response', response[0])
                        return callback({
                            status: false,
                            message:'response is null'
                        });
                    }
                return callback({
                    status: true,
                    message: 'OTP check successful',
                    response:response[0]});
            })
        } catch (err) {
            console.log('Myerr', err)
            return callback({
                status: false,
                message: 'failed OTP check',});
        }
    }

    // reset password
    reset_password(phone, password,callback){
        let query = "UPDATE users SET password='"+password+ "' WHERE phone=" +phone;
        try {
            db.query(query, (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                if(response.changedRows==0){
                    return callback({
                        status: false,
                        message:'Wrong phone number'
                    });
                }
                return callback({
                    status: true,
                    message: 'reset password successful',
                    response:response});
            })
        } catch (err) {
            console.log('Myerr', err)
            return callback({
                status: false,
                message: 'failed password reset'});
        }
    }

    // UPDATE PROFILE --------------------------------------------------------------
    update_profile(first_name, last_name, pefum_code,callback){
        if(!first_name|| !last_name){
            return callback({
                status:false,
                message:'Parameters cannot be empty'
            })
        }
        let query = 'UPDATE `users` SET `first_name`= "'+first_name+'", `last_name`="'+last_name+'" WHERE `pefum_code`=' +pefum_code;
         try {
            db.query(query, (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                if(response.changedRows==0){
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                return callback({
                    status: true,
                    message: 'Profile update successful'});
            })
        } catch (err) {
            console.log('Myerr', err)
            return callback({
                status: false,
                message: 'failed profile update'});
        }
    }

    // TRANSACTIONS SECTION (pefum and coupon transaction merged)------------------------------------
    // ----------------------------------------------------------------------------------------
    get_transaction(pefum_code, callback){
        let query = 
        "SELECT `transaction_id`,`pefum_code`,`coupon_code`, `attendant_code`, fuel_station_id, station_branch_id, amount, fuel_type, `status`,my_date, my_time FROM `coupon_transactions` \
        WHERE pefum_code='" + pefum_code +
        "' UNION \
        SELECT `transaction_id`,`pefum_code`,`coupon_code`,`attendant_code`, fuel_station_id, station_branch_id, amount, fuel_type, `status`,my_date, my_time FROM `pefum_transactions` \
        WHERE pefum_code='" + pefum_code + "' ORDER BY my_time" ;
    
        try {
            // get all transaction in both pefum transactions and coupon transactions
        db.query(query,(err, result)=>{
            if(err){ return callback({status: false,message: 'error here',}); }  
            if(response.length == 0){ 
                return callback({
                    status: false,
                    message:'response is null'
                });
            }
            
            var station_data = []; //stores all the station branch data
            var transactions = []; //stores custom formated transaction + station branch station_data
            var pending = result.length; //used to hold transaction length
            for(var i in result){//loop thru each transaction and use the station_branch_id to get station info
                let fquery = 'SELECT `company_name`,`location`,`logo`,`station_branch_id` FROM `station_branches` WHERE `station_branch_id`=' + result[i].station_branch_id;
                db.query(fquery, (err, response)=>{// get station_branch info
                    if(err){ return callback({status: false,message: 'error here',}); }  
                    if(response.length == 0){
                        console.log('response', response[0])
                        return callback({
                            status: false,
                            message:'response is null'
                        });
                    }

                    //stores each station branch response
                    var station_branch = response[0]
                     //adds to arraylist
                    station_data.push(station_branch);
                    if(0 == --pending){// only works when final push has been made to 'station_data' arraylist
                        for(var j in result){
                            var trans_obj = {
                                transaction_id: result[j].transaction_id,
                                pefum_code: result[j].pefum_code,
                                coupon_code:result[j].coupon_code,
                                attendant_code: result[j].attendant_code,
                                fuel_station_id: result[j].fuel_station_id,
                                station_branch_id: result[j].station_branch_id,
                                station_branch_id2:station_data[j].station_branch_id,
                                company_name:station_data[j].company_name,
                                location: station_data[j].location,
                                logo: station_data[j].logo,
                                amount: result[j].amount,
                                fuel_type: result[j].fuel_type,
                                status: result[j].status,
                                my_date: result[j].my_date,
                                my_time: result[j].my_time
                                } 
                            //stores newly formated transaction data in 'transactions' array
                            transactions.push(trans_obj);
                        } 
                        // sends all transaction to route
                        return callback({
                            status:true,
                            response:transactions,
                            message:'All transactions requested'
                        })
                    }
                }) 
              }
            })
        } catch (error) {
            return callback({
                status:false, 
                message:'failed to request all transactions'
            })
        }
    }
    // -----------------------------------------------------------------------------------------


    //get fuel station basic info
     get_fuel_station_info(station_branch_id, callback){
         
        let query = 'SELECT `company_name`,`location`,`logo` FROM `station_branches` WHERE `station_branch_id`=' + station_branch_id;
        try {
            db.query(query, (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(response.length == 0){ 
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                // console.log('station DATA', response[0])
                return new Promise((resolve, reject)=>{
                    resolve({
                        status:true,
                        response:response[0]})

                    reject(err)
                })
            })
        } catch (err) {
            return callback({
                status: false,
                message:'response is null'
            });
        }
    }

    //PENDING TRANSACTIONS ---------------------------------------------------------------
    get_pending_transaction(pefum_code, callback){
        let query = 
        "SELECT `transaction_id`,`pefum_code`,`coupon_code`, `attendant_code`, fuel_station_id, station_branch_id, amount, fuel_type, `status`,my_date, my_time FROM `coupon_transactions` WHERE pefum_code='" + pefum_code +
        "' AND status='pending' UNION SELECT `transaction_id`,`pefum_code`,`coupon_code`,`attendant_code`, fuel_station_id, station_branch_id, amount, fuel_type, `status`,my_date, my_time FROM `pefum_transactions` WHERE pefum_code='" + pefum_code + "' AND status='pending' ORDER BY my_time" ;
        
        try {
            // get all transaction in both pefum transactions and coupon transactions
            db.query(query,(err, result)=>{ 
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(result.length == 0){  
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                } 
               if(result.length!=0){ 
                        var station_data = []; //stores all the station branch data
                        var transactions = []; //stores custom formated transaction + station branch station_data
                        var pending = result.length; //used to hold transaction length
                        for(var i in result){//loop thru each transaction and use the station_branch_id to get station info
                            
                            let fquery = 'SELECT `company_name`,`location`,`logo`,`station_branch_id` FROM `station_branches` WHERE `station_branch_id`=' + result[i].station_branch_id;
                            db.query(fquery, (err, response)=>{// get station_branch info
                                if(err){ return callback({status: false,message: 'error here',}); }  
                                if(response.length == 0){
                                    // console.log('response', response[0])
                                    return callback({
                                        status: false,
                                        message:'response is null'
                                    });
                                }
                                if(response!=null){ 
                                    //stores each station branch response
                                    var station_branch = response[0]
                                    //adds to arraylist
                                    station_data.push(station_branch);
                                    if(0 == --pending){// only works when final push has been made to 'station_data' arraylist
                                        for(var j in result){
                                            var trans_obj = {
                                                transaction_id: result[j].transaction_id,
                                                pefum_code: result[j].pefum_code,
                                                coupon_code:result[j].coupon_code,
                                                attendant_code: result[j].attendant_code,
                                                fuel_station_id: result[j].fuel_station_id,
                                                station_branch_id: result[j].station_branch_id,
                                                station_branch_id2:station_data[j].station_branch_id,
                                                company_name:station_data[j].company_name,
                                                location: station_data[j].location,
                                                logo: station_data[j].logo,
                                                amount: result[j].amount,
                                                fuel_type: result[j].fuel_type,
                                                status: result[j].status,
                                                my_date: result[j].my_date,
                                                my_time: result[j].my_time
                                                } 
                                            //stores newly formated transaction data in 'transactions' array
                                            transactions.push(trans_obj);
                                        } 
                                        // sends all transaction to route
                                        return callback({
                                            status:true,
                                            response:transactions,
                                            message:'All pending transactions requested'
                                        })
                                    }
                                }else{
                                    return callback({
                                        status:false,
                                        message:'no data'
                                    })
                                }
                            }) 
                        }
               }else{ 
                   return callback({
                       status:false,
                       message:'no data'
                   })
               }
            })
        } catch (error) { 
            return callback({
                status:false, 
                message:'failed to request all pending transactions'
            })
        }

    }

// APPROVE PENDING TRANSACTION----------------------------------------------------------------------------------
    approve_transaction(transaction_id, pefum_code, callback){
        let query = 'SELECT `transaction_id`,`pefum_code`,`coupon_code`, `attendant_code`, fuel_station_id, station_branch_id, amount, fuel_type, `status`,my_date, my_time FROM pefum_transactions WHERE transaction_id='+transaction_id +' AND status="pending" UNION \
        SELECT `transaction_id`,`pefum_code`,`coupon_code`, `attendant_code`, fuel_station_id, station_branch_id, amount, fuel_type, `status`,my_date, my_time FROM coupon_transactions WHERE transaction_id='+transaction_id +' AND status="pending"';
        try {
            //gets all transaction by merging pefum and coupon transactions
            db.query(query, (err, response)=>{
                // console.log('pending transaction:', response)
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(response.length == 0){ 
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                if(response!=null){
                    //gets current wallet balance from either PEFUM wallet or COUPON wallet
                    this.get_wallet_balance(pefum_code,response[0].coupon_code||response[0].pefum_code, (result)=>{
                         
                        if(result.status==true){ 
                            // console.log('wallet result:', result)
                            var amount = parseFloat(response[0].amount);
                            var old_balance =parseFloat(result.response.pefum_wallet || result.response.coupon_wallet);
                            // console.log('fuel price:', amount)
                            // console.log('wallet balance:', old_balance)
                            // check if wallet balance is greater than the fuel price
                            if(old_balance>amount){
                                var new_balance = old_balance - amount;
                                //update user wallet with new balance
                                this.update_wallet(pefum_code,response[0].coupon_code||response[0].pefum_code,new_balance, (value)=>{
                                    if(value.status==true){
                                        console.log('New wallet balance', new_balance)
                                    //updates the transaction status to complete, so it doesn't show on pending list
                                    this.update_transaction_status(transaction_id,'completed', (data)=>{
                                        if(data.status==true){
                                            return callback({
                                                status:true,
                                                message:'Transaction Approved',
                                                value});
                                        }else{
                                            return callback({
                                                status:false,
                                                message:data.message
                                                });
                                        }
                                    })
                                    }else{
                                        return callback({
                                            status:false,
                                            message:value.message
                                            });
                                    }
                                })
                            }
                            else{
                                //updates the transaction status to low-balance, so it doesn't show on pending list
                                this.update_transaction_status(transaction_id,'low-balance', (data)=>{
                                    return callback({
                                        status:false,
                                        message:'Not enough wallet balance for transaction'
                                        });
                                });
                            }
                        }else{
                            return callback({
                                status:false,
                                message:result.message
                            })
                        }
                    })
                }
            })
        } catch (err) {
            console.log('Myerr', err)
            return callback({
                status: false,
                message: 'failed to approve transaction'});
        }
    }

    // DECLINE PENDING TRANSACTION----------------------------------------------------------------------------------
    decline_transaction(transaction_id, callback){
        let query = "SELECT transaction_id FROM pefum_transactions WHERE transaction_id='"+transaction_id +"' AND status='pending' UNION \
        SELECT transaction_id FROM coupon_transactions WHERE transaction_id='"+transaction_id +"' AND status='pending'";
        try {
            //gets all transaction by merging pefum and coupon transactions
            db.query(query, (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                if(response!=null){ 
                    //updates the transaction status to declined, so it doesn't show on pending list
                    this.update_transaction_status(transaction_id,'declined', (data)=>{
                        if(data.status==true){
                            return callback({
                                status: true,
                                message: 'Transaction decline successful',}); 
                        }
                        else{
                            return callback({
                                status: false,
                                message: 'failed to decline transaction'});
                        }
                    });
                }
            })
        } catch (err) {
            console.log('Myerr', err)
            return callback({
                status: false,
                message: 'failed to decline transaction'});
        }
    }


//UPDATE USER PENDING STATUS
 //change pending status to completed or 0 in users
 update_user_pending_status(pefum_code, callback){
    let query = 'UPDATE users SET pending_status=0 WHERE pefum_code='+pefum_code;
    try {
        db.query(query, (err, response)=>{
            if(err){ return callback({status: false,message: 'error here',}); }  
            if(response.length == 0){
                console.log('response', response[0])
                return callback({
                    status: false,
                    message:'user pending status had issues'
                });
            }
            
            if(response !=null){
                return callback({
                    status:true, 
                    message:'User pending_status updated successfully'
                })
            }
            else{
                return callback({
                    status:false,
                    message:'Failed to update User pending_status'
                })
            }
        })
    } catch (err) {
        return callback({
            status:false,
            message:'Failed to update User pending_status'
        })
    }
}



// UPDATE TRANSACTION STATUS TO COMPLETED
    update_transaction_status(transaction_id, status, callback){
        let fquery = 'UPDATE pefum_transactions SET status= "'+status+'" WHERE transaction_id='+ transaction_id;
        let cquery = 'UPDATE coupon_transactions SET status= "'+status+'" WHERE transaction_id='+ transaction_id;
        try {
            db.query(fquery, (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }   
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'xx response is null'
                    });
                }
                
                if(response.changedRows >0){
                    return callback({
                        status: true,
                        message: 'Transaction status = completed'
                    }) 
                }else{
                    db.query(cquery, (err, result)=>{
                        if(err){ return callback({status: false,message: 'error here',}); }  
                        if(result.changedRows>0){
                            console.log('pending', 'transaction update')
                            return callback({
                                status: true,
                                message: 'Transaction status = completed'
                            }) 
                        }
                        else{
                            return callback({
                                status: false,
                                message: 'Transaction status not updated'
                            })
                        }
                    })
                }
            })
        } catch (err) {
            console.log('Myerr', err)
        }
    }

// GETs PEFUM OR COUPON WALLET BALANCE---------------------------------Internal Use only---------------------------
    get_wallet_balance(pefum_code, pefum_or_coupon, callback){
        let query = 'SELECT pefum_wallet FROM users WHERE pefum_code='+pefum_code;
        //checks whether to return pefum wallet of coupon wallet balance
        if(pefum_code==pefum_or_coupon){//this means return pefum wallet balance
            try {
                db.query(query, (err, response)=>{
                    if(err){ return callback({status: false,message: 'error here',}); }   
                    if(response.length == 0){ 
                        return callback({
                            status: false,
                            message:'response is null'
                        });
                    }
                    if(response !=null){
                        console.log('current wallet balance', response[0])
                        return callback({
                            status:true,
                            message:'Pefum wallet returned',
                            response:response[0]});
                    }
                })
            } catch (err) {
                console.log('Myerr', err)
                return callback({
                    status:false,
                    message:'failed to get pefum wallet'
                })
            }
        }else if(pefum_code!=pefum_or_coupon){//this means return coupon wallet balance
            //get coupon wallet balance
            this.get_coupon_balance(pefum_code,(result)=>{
                if(result.status==true){
                    return callback({
                        status:true,
                        message:'Coupon wallet returned',
                        response:result.response
                    })
                }
            })
        }else{
            return callback({
                status:false,
                message:'failed to get coupon wallet'
            })
        }
    }

    // GET COUPON WALLET BALANCE ONLY---------------------------------------------------------------------------------
    get_coupon_balance(pefum_code, callback){
        let query = 'SELECT coupon_wallet FROM users WHERE pefum_code='+pefum_code;
        try {
            db.query(query, (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(response.length == 0){ 
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                if(response !=null){
                    // console.log('current wallet balance', response[0])
                    return callback({
                        status: true,
                        response:response[0],
                        message:'Coupon wallet request successful'
                    });
                }
            })
        } catch (err) {
            console.log('Myerr', err)
            return callback({
                status: false,
                message:'err getting coupon wallet'
            });
        }
    }

// UPDATE WALLET BALANCE(pefum wallet or coupon wallet)---------------------------------------------------------------------------------------
    update_wallet(pefum_code,pefum_or_coupon, new_balance, callback){
        let query = 'UPDATE users SET pefum_wallet=' +new_balance+ ' WHERE pefum_code='+ pefum_code;

         //checks whether to update pefum wallet or coupon wallet balance
         if(pefum_code==pefum_or_coupon){//this means update pefum wallet 
            try {
                db.query(query, (err, response)=>{
                    if(err){ return callback({status: false,message: 'error here',}); }   
                    if(response.length == 0){ 
                        return callback({
                            status: false,
                            message:'sa response is null'
                        });
                    }
                    if(response.changedRows==0){
                        return callback({
                            status: false,
                            message: 'dds response is null'
                        }) 
                    }
                    
                    if(response !=null){
                        // console.log('pefum wallet balance updated', response)
                        return callback({
                            status: true,
                            message: 'pefum wallet balance updated'});
                    }
                })
            } catch (err) {
                console.log('Myerr', err)
                return callback({
                    status: false,
                    message: 'pefum wallet failed to update'});
            }
        }else if(pefum_code!=pefum_or_coupon){//this means update coupon wallet
            this.update_coupon_wallet(pefum_code, new_balance,(response)=>{
                if(response.status==true){
                    return callback({
                        status: true,
                        message: 'coupon wallet balance updated'});
                }else{
                    return callback({
                        status: false,
                        message: 'coupon wallet failed to update'});
                }
            })
        }else{
            return callback({
                status: false,
                message: 'coupon wallet failed to update'});
        }
    }

    //update coupon wallet(both user and employee coupon wallet)--------------Internal use only----------------------
 update_coupon_wallet(pefum_code, new_balance, callback){
        let query = 'UPDATE users SET coupon_wallet=' +new_balance+ ' WHERE pefum_code='+ pefum_code;
        let emp_query = 'UPDATE employees SET wallet=' +new_balance+ ' WHERE pefum_code='+ pefum_code;
        try {
            //updates the users coupon wallet
            db.query(query, (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(response.length == 0){
                    return callback({
                        status: false,
                        message:'sas response is null'
                    });
                }
                if(response.changedRows==0){
                    return callback({
                        status: false,
                        message:'aaa response is null'
                    });
                }
                
                    // console.log('response Usr', response)
                    //update the employee coupon wallet too
                    try {
                        db.query(emp_query,(err, result)=>{
                            if(err){ return callback({status: false,message: 'error here',}); }  
                            if(response.length == 0){ 
                                return callback({
                                    status: false,
                                    message:'sdf response is null'
                                });
                            }
                            if(result.changedRows>0){
                                // console.log('response USR', response.changedRows)
                                // console.log('response EMP', result.changedRows)
                                return callback({
                                    status: true,
                                    message: 'Coupon wallet balance updated'});
                            } 
                        })
                    } catch (err) {
                        return callback({
                            status: false,
                            message: 'Coupon wallet failed to update'});
                    }
                 
            })
        } catch (err) {
            console.log('Myerr', err)
            return callback({
                status: false,
                message: 'wallet failed to update'});
        }
    }



// DEPOSITE SECTION---------------------------------------------------------------------------------------------
    add_deposite(pefum_code, amount, payment_method, first_name, last_name, phone, card_details, transaction_id, message, status, date, time, callback){
        let query = "INSERT INTO `deposites` (`pefum_code`, `amount`, `payment_method`, `first_name`, `last_name`, `phone`, `card_details`, `transaction_id`, `message`, `status`, `date`, `time`) VALUES (?)";
        var values = [pefum_code, amount, payment_method, first_name, last_name, phone, card_details, transaction_id, message, status, date, time];
        
        try {
            db.query(query, [values], (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(response.length == 0){
                    return callback({
                        status: false,
                        message:' response is null'
                    });
                }
                
                if(response!=null){
                    console.log('response', response)
                    return callback({
                        status: true,
                        message: 'Added to deposites database'});
                }
                else{
                    return callback({
                        status: false,
                        message: 'Failed to add to deposites database'});
                }
            })
        } catch (err) {
            console.log('Myerr', err)
            return callback({
                status: false,
                message: 'Failed to add to deposites database'});
        }
    }
}


module.exports = client_db;