const db = require('./database.js')



class attendant_db{
    constructor(){
        global.db = db;
    }

    // LOGIN SECTION ---------------------------------------------------------------------------
    get_attendant(phone, callback){
        let query = "SELECT * FROM attendants WHERE phone=" + phone;
            if(!Number.isNaN(phone)){
                try {
                    db.query(query, (err, response)=>{
                        if(err){ return callback({status: false,message: 'error here',}); }  
                   
                        if(response.length ==1){
                            return callback({
                                status:true,
                                message:'Attendant correct',
                                response:response[0]});
                        }
                        else{
                            return callback({
                                status: false,
                                message:'no data'});
                        }
                    })
                } catch (error) {
                    return callback({
                        status: false,
                        message:'no data'});
                }
            }
            else {
                return callback({
                    status: false,
                    message:'no data'});
            }
    }

    
    // TRANSACTIONS SECTION (pefum and coupon transaction merged)------------------------------------
    get_transaction(attendant_code, callback){
        let query = 
        "SELECT transaction_id,attendant_code,pefum_code, pefum_code,coupon_code, fuel_station_id, station_branch_id, amount, fuel_type, `status`,my_date, my_time FROM `coupon_transactions` \
        WHERE attendant_code=" + attendant_code +
        " UNION \
        SELECT transaction_id,attendant_code,pefum_code, pefum_code,coupon_code, fuel_station_id, station_branch_id, amount, fuel_type, `status`,my_date, my_time FROM `pefum_transactions` \
        WHERE attendant_code=" + attendant_code + " ORDER BY my_time" ;
        var TRANSACTION= [];// all reformated transactions stored here
        try {//get all pefum and coupon transactions
            db.query(query, (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(response.length == 0){
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                // console.log('response k', response)
                var users = [];//store all users for each transaction here
                var pending = response.length;
                for( var i in response){//loop thru all raw transactions
                    this.get_user_info(response[i].pefum_code, (result)=>{//get each user info
                        if(result.status==true){ 
                            users.push(result) //add user info to array
                            if(0 == --pending){//when for loop is finished
                                for(var j in response){//loop thru again
                                    var obj={// custom transaction(reformated)
                                        'transaction_id':response[j].transaction_id,
                                        'pefum_code':response[j].pefum_code,
                                        'attendant_code':response[j].attendant_code,
                                        'fuel_station_id':response[j].fuel_station_id,
                                        'station_branch_id':response[j].station_branch_id,
                                        'amount':response[j].amount,
                                        'fuel_type':response[j].fuel_type,
                                        'status':response[j].status,
                                        'first_name':users[j].response.first_name,
                                        'last_name':users[j].response.last_name,
                                        'profile_image':users[j].response.profile_image,
                                        'date':response[j].my_date,
                                        'time':response[j].my_time
    
                                    }
                                    TRANSACTION.push(obj);//new reformated transaction added to array
                                }
                                console.log('transaction', TRANSACTION)
                                return callback({
                                    status:true,
                                    response:TRANSACTION,
                                    message:'Transactions requested successful'
                                }) 
                            }
                        }else{
                            return callback({
                                status:false,
                                message:'no transaction data'
                            })
                        }
                    })
                }
            })
        } catch (error) {
            console.log('MyError', error)
            return callback({
                status:false,
                message:'error getting transactions'});
       
        }
    }

    //get user basic info  -------- QRCODE get basic user info
    get_user_info(pefum_code, callback){
        let query = 'SELECT pefum_code,first_name,last_name,profile_image FROM users WHERE pefum_code=' + pefum_code;
        try {
            db.query(query, (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                return callback({
                    status:true,
                    response:response[0],
                    message:'User data obtained successfully'
                })
            })
        } catch (error) {
            console.log('MyError', error)
            return callback({
                status:false, 
                message:'Failed to get user data'
            })
        }
    }

    // CREATE TRANSACTION-------------------------------------------------------------------
    //pefum transaction
    pefum_transaction(attendant_code,pefum_code,fuel_station_id,station_branch_id,amount,fuel_type,transaction_id,status,date,time,callback){
        let query = 'INSERT INTO pefum_transactions (attendant_code,pefum_code,fuel_station_id,station_branch_id,amount,fuel_type,transaction_id,status,my_date,my_time) \
        VALUES (?)' ;
        var values = [attendant_code,pefum_code,fuel_station_id,station_branch_id,amount,fuel_type,transaction_id,status,date,time];
        try {
            db.query(query,[values], (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                if(response !=null){
                    return callback({
                        status:true,
                        message:'Transaction created successfully using pefum code'
                    })
                }
                else{
                    return callback({
                        status:false,
                        message:'Failed to create transaction using pefum_code'
                    })
                }
            })
        } catch (error) {
            return callback({
                status:false,
                message:'Failed to create transaction using pefum_code'
            })
        }
    }

    //coupon transaction
    coupon_transaction(attendant_code,coupon_code,fuel_station_id,station_branch_id,amount,fuel_type,transaction_id,status,date,time,callback){
        //get basic employee info to insert into coupon transaction db
        this.get_employee_info(coupon_code,(result)=>{
            if(result.status !=false){
                var employee_info = result.response;
                var pefum_code = employee_info.pefum_code;
                var corp_id = employee_info.corp_id;
                var corp_branch_id = employee_info.corp_branch_id;
                let query = 'INSERT INTO coupon_transactions (pefum_code,corp_id,corp_branch_id,attendant_code,coupon_code,fuel_station_id,station_branch_id,amount,fuel_type,transaction_id,status,my_date,my_time) \
                VALUES (?)' ;
                var values = [pefum_code,corp_id,corp_branch_id,attendant_code,coupon_code,fuel_station_id,station_branch_id,amount,fuel_type,transaction_id,status,date,time];
                try {
                    db.query(query,[values], (err, response)=>{
                        if(err){ return callback({status: false,message: 'error here',}); }  
                        if(response.length == 0){ 
                            return callback({
                                status: false,
                                message:'1 response is null'
                            });
                        }
                        if(response !=null){
                            return callback({
                                status:true,
                                message:'Transaction created successfully using coupon'
                            })
                        }
                        else{
                            return callback({
                                status:false,
                                message:'Failed to create transaction using coupon_code'
                            })
                        }
                    })
                } catch (error) {
                    return callback({
                        status:false,
                        message:'Failed to create transaction using coupon_code'
                    })
                }
            }
            else{
                return callback({
                    status:false,
                    message:'Failed to create transaction using coupon_code'
                })
            }
        })
    }

    // TRIGGER PENDING STATUS IN USERS
    //using pefum_code to trigger pending status to pending or 1 in users
    pefum_trigger(pefum_code, callback){
        let query = 'UPDATE users SET pending_status=1 WHERE pefum_code='+pefum_code;
        try {
            db.query(query, (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(response.length == 0){ 
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                if(response.changedRows==0){
                    return callback({
                        status: true,
                        message:' pending_status not updated (maybe already pending)'
                    });
                }
                if(response !=null){
                    return callback({
                        status:true, 
                        message:'User pending_status updated successfully'
                    })
                }
                else{
                    return callback({
                        status:false,
                        message:'Failed to update User pending_status'
                    })
                }
            })
        } catch (error) {
            return callback({
                status:false,
                message:'Failed to update User pending_status'
            })
        }

    }
    //using coupon_code to trigger pending status to pending or 1 in users
    coupon_trigger(coupon_code, callback){
        let query = 'UPDATE users SET pending_status=1 WHERE coupon_code='+coupon_code;
        try {
            db.query(query, (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'response is null'
                    });
                }
                if(response.changedRows==0){
                    return callback({
                        status: true,
                        message:' pending_status not updated (maybe already pending)'
                    });
                }
                if(response !=null){
                    console.log('true response', response)
                    return callback({
                        status:true, 
                        message:'User pending_status updated successfully'
                    })
                }
                else{
                    return callback({
                        status:false,
                        message:'Failed to update User pending_status'
                    })
                }
            })
        } catch (error) {
            return callback({
                status:false,
                message:'Failed to update User pending_status'
            })
        }
    }

    // GET EMPLOYEE BASIC INFO------------------------------------------------------------
    get_employee_info(coupon_code,callback){
        let query= 'SELECT pefum_code,corp_id,corp_branch_id FROM employees WHERE coupon_code='+coupon_code;
        try {
            db.query(query,(err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'q response is null'
                    });
                }
                if(response !=null){
                    return callback({
                        status:true,
                        response:response[0],
                        message:'Employee data obtained successfully'
                    })
                }
                else{
                    return callback({
                        status:false,
                        message:'Failed to get employee info'
                    })
                }
            })
        } catch (error) {
            return callback({
                status:false,
                message:'Failed to get employee info'
            })
        }
    }

     // CANCEL PENDING TRANSACTION----------------------------------------------------------------------------------
     cancel_transaction(transaction_id, callback){
        let query = "SELECT transaction_id FROM pefum_transactions WHERE transaction_id='"+transaction_id +"' AND status='pending' UNION \
        SELECT transaction_id FROM coupon_transactions WHERE transaction_id='"+transaction_id +"' AND status='pending'";
        try {
            //gets all transaction by merging pefum and coupon transactions
            db.query(query, (err, response)=>{
                if(err){ return callback({status: false,message: 'error here',}); }  
                if(response.length == 0){
                    console.log('response', response[0])
                    return callback({
                        status: false,
                        message:'no pending transaction available'
                    });
                }
                if(response!=null){ 
                    //updates the transaction status to declined, so it doesn't show on pending list
                    this.update_transaction_status(transaction_id,'cancel', (data)=>{
                        if(data.status==true){
                            return callback({
                                status: true,
                                message: 'Transaction cancelled successful',}); 
                        }
                        else{
                            return callback({
                                status: false,
                                message: 'failed to cancelled transaction'});
                        }
                    });
                }
            })
        } catch (err) {
            console.log('Myerr', err)
            return callback({
                status: false,
                message: 'failed to decline transaction'});
        }
    }

    
// UPDATE TRANSACTION STATUS TO COMPLETED
update_transaction_status(transaction_id, status, callback){
    let fquery = 'UPDATE pefum_transactions SET status= "'+status+'" WHERE transaction_id='+ transaction_id;
    let cquery = 'UPDATE coupon_transactions SET status= "'+status+'" WHERE transaction_id='+ transaction_id;
    try {
        db.query(fquery, (err, response)=>{
            if(err){ return callback({status: false,message: 'error here',}); }   
            if(response.length == 0){
                console.log('response', response[0])
                return callback({
                    status: false,
                    message:'xx response is null'
                });
            }
            
            if(response.changedRows >0){
                return callback({
                    status: true,
                    message: 'Transaction status = cancelled'
                }) 
            }else{
                db.query(cquery, (err, result)=>{
                    if(err){ return callback({status: false,message: 'error here',}); }  
                    if(result.changedRows>0){
                        console.log('pending', 'transaction update')
                        return callback({
                            status: true,
                            message: 'Transaction status = cancelled'
                        }) 
                    }
                    else{
                        return callback({
                            status: false,
                            message: 'Transaction status not updated'
                        })
                    }
                })
            }
        })
    } catch (err) {
        console.log('Myerr', err)
    }
}

}


module.exports = attendant_db;