const { response } = require('express');
const mysql = require('mysql');

var db;

function connection(){
    const db = mysql.createConnection({
        host: '127.0.0.1',
        user: 'root',
        password: '',
        database: 'pfmdb'
    });

    db.connect((err)=>{
        if(err){
            throw err;
        }
        console.log('Connection', 'Success for all routes');
    });

    return db;
}

module.exports = connection();
    
    