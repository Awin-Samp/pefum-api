const express = require('express');
const bcrypt = require('bcrypt');
require('dotenv/config');
const jwt = require('jsonwebtoken');
const random = require('random');
const bodyparser = require('body-parser');
const verifyToken = require('../verifyToken'); 

const router  = express.Router();

//corporate_db class
const corporate_db = require('../models/corporate_db.js');

// instatiate corporate_db
let Corporate = new corporate_db();

//Set up middlewares
router.use(bodyparser.json());
router.use(bodyparser.urlencoded({extended: false}));

//REGISTRATION SECTION
router.post('/register',(res, req)=>{
    const corp_id = 'corp'+random.int(100,100000);
    var address = req.body.address;
    var name = req.body.name;
    var login_name = req.body.login_name;
    var password = req.body.password;
    var phone = req.body.phone; 
    var email = req.body.email; 
    var logo = 'logo.png';
    var active = false;
    // date and time
    const oldDate = new Date()
    var date = oldDate.toISOString().split('T')[0];
    var time  = new Date().toLocaleTimeString();  

    //register account
    Client.register_account(corp_id, name, logo, login_name, password, phone, email, address, active, date, time, (response)=>{
        if(response.status==true){
            res.status(200).json({
                status:true,
                message:response.message,
                other: 'contact pfm admin to activate account'
            })
        }else{
            res.status(500).json({
                status:false,
                message:response.message
            })
        }
    })
})


//LOGIN SECTION
router.post('/login', async (req, res)=>{
    var login_name = req.body.login_name;
    var password = req.body.password;
    // get attendant from attendantDB
    Corporate.get_corporate(login_name, (response)=>{ 
            if(response.status==true){
                bcrypt.compare(password, response.password)
                    .then((result)=>{
                        // generate token here with UserID 
                            var token = jwt.sign({UserID: response.corp_id }, process.env.SECRET_KEY);
                    
                            if(result !=null){
                                //check if account is active or deactivated
                                if(response.active==true){
                                    return res.send({
                                        status: true,
                                        token: token,
                                        message: 'Corporate login successful',
                                        user: response
                                    }) 
                                }else{
                                    return res.send({
                                        status: false,
                                        message:'Account is deactivated (contact pfm admin)',
                                        other: 'Corporate login details is correct',
                                        user: response
                                    }) 
                                }
                            }
                    }).catch((err)=>{ 
                        res.status(404).send({
                            status: false, 
                            message: 'password error!'
                        })
                        return;
                        })
            }
            else{
                res.status(404).json({
                    status: false,
                    message: "login name can't be empty!"
                }) 
            } 
    });
});


//CREATE NEW BRANCH
router.post('/create_branch', verifyToken, (req, res)=>{
    var corp_id = req.UserID; //get station ID
    var address = req.body.address;
    var name = req.body.name;
    var login_name = req.body.login_name;
    var password = req.body.password;
    var phone = req.body.phone;
    var wallet = req.body.wallet;
    var email = req.body.email;
    var logo = 'logo.png';
    var active = true;
    // date and time
    const oldDate = new Date()
    var date = oldDate.toISOString().split('T')[0];
    var time  = new Date().toLocaleTimeString();  

    //Hashing the password
    const hashedPassword = bcrypt.hashSync(password, 10);
    //create custom corp branch id
    const corp_branch_id = random.int(100,100000);
    //create new branch
    Corporate.create_branch(corp_branch_id,corp_id,name,logo,login_name,hashedPassword,wallet,phone,email,address,active,date,time, (response)=>{
        if(response.status==true){
            res.status(200).json({
                status: true,
                message:response.message,
                response:response.response
            })
        }else{
            res.status(500).json({
                status:false,
                message:response.message
            })
        }
    })
});



//GET ALL BRANCH
router.get('/all_branches', verifyToken, (req, res)=>{
    var corp_id = req.UserID; //get station ID

    //get all branches using corp_id
    Corporate.get_all_branches(corp_id, (response)=>{
        if(response.status==true){
            res.status(200).json({
                status:true,
                message:response.message,
                response:response.response
            })
        }else{
            res.status(404).json({
                status:false,
                message:response.message
            })
        }
    })
   
});

//GET ONE BRANCH
router.get('/single_branch/:id', verifyToken, (req, res)=>{
    var corp_id = req.UserID; //get station ID
    var corp_branch_id = req.params.corp_branch_id;

    //get single branch
    Corporate.get_branch(corp_branch_id, (response)=>{
        if(response.status==true){
            res.status(200).json({
                status:true,
                message:response.message,
                response:response.response
            })
        }else{
            res.status(404).json({
                status:false,
                message:response.message
            })
        }
    })
});


//DELETE ONE BRANCH\
router.get('/delete_branch/:id', verifyToken, (req, res)=>{
    var corp_id = req.UserID; //get station ID
    var corp_branch_id = req.params.corp_branch_id;

    //delete one branch using corp_branch_id
    Corporate.delete_branch(corp_branch_id, (response)=>{
        if(response.status==true){
            res.status(200).json({
                status:true,
                message:response.message, 
            })
        }else{
            res.status(404).json({
                status:false,
                message:response.message
            })
        }
    })
});



//ACTIVATE/DEACTIVATE BRANCH 
router.get('/activate_deactivate_branch/:id', verifyToken, (req, res)=>{
    var corp_id = req.UserID; //get station ID
    var corp_branch_id = req.params.id;

    //activates or deactivates depending on the current active-state
    Corporate.activate_deactivate_branch(corp_branch_id, (response)=>{
        if(response.status==true){
            res.status(200).json({
                status:true,
                message:response.message, 
            })
        }else{
            res.status(404).json({
                status:false,
                message:response.message
            })
        }
    })
});



//GET  ALL TRANSACTIONS
router.get('/all_transaction', verifyToken, (req, res)=>{
    var corp_id = req.UserID;
    //gets all transaction under corporate
    Corporate.get_transactions(corp_id, (response)=>{
        if(response.status==true){
            res.status(200).json({
                status:true,
                message:response.message, 
                response:response.response
            })
        }else{
            res.status(404).json({
                status:false,
                message:response.message
            })
        }
    })
   
});



//GET  employee TRANSACTIONS 
router.get('/employee_transactions/:coupon_code', verifyToken, (req, res)=>{
    var corp_id = req.UserID;
    var coupon_code = req.params.coupon_code;

    //get all transactions using coupon_code
    Corporate.get_employee_transaction(coupon_code, (response)=>{
        if(response.status==true){
            res.status(200).json({
                status:true,
                message:response.message,
                response:response.response 
            })
        }else{
            res.status(404).json({
                status:false,
                message:response.message
            })
        }
    })
});



//GET  BRANCH TRANSACTIONS 
router.get('/branch_transactions/:corp_branch_id', verifyToken, (req, res)=>{
    var corp_id = req.UserID;
    var corp_branch_id = req.params.corp_branch_id;

    //get all transactions using corp_branch_id
    Corporate.get_branch_transactions(corp_branch_id, (response)=>{
        if(response.status==true){
            res.status(200).json({
                status:true,
                message:response.message,
                response:response.response 
            })
        }else{
            res.status(404).json({
                status:false,
                message:response.message
            })
        }
    })
});



//GET ONE TRANSACTION
router.get('/single_transaction/:transaction_id', verifyToken, (req, res)=>{
    var corp_id = req.UserID;
    var transaction_id = req.params.transaction_id;

    //get transaction using transaction id 
    Corporate.get_single_transaction(transaction_id, (response)=>{
        if(response.status==true){
            res.status(200).json({
                status:true,
                message:response.message,
                response:response.response 
            })
        }else{
            res.status(404).json({
                status:false,
                message:response.message
            })
        }
    })
});


//CREATE employee
router.post('/create_employee', verifyToken, async (req, res)=>{
    var corp_id = req.UserID;
    var corp_name = req.body.corp_name;
    var corp_branch = req.body.corp_branch;
    var corp_branch_id = req.body.branch_id;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name; 
    var phone = req.body.phone;
    var pefum_code = req.body.pefum_code; 
    var top_up = req.body.top_up;
    var fuel_station_subscribed = req.body.fuel_station_subscribed; 
    var active = req.body.active;
    // date and time
    const oldDate = new Date()
    var date = oldDate.toISOString().split('T')[0];
    var time  = new Date().toLocaleTimeString();  

    //create a coupon code
    var coupon_code = random.int(10000,1000000)


    var topup = parseFloat(top_up)
    var wallet = topup; //wallet is the same amount as top_up since this is a new employee
    
    //check if corp_wallet is greater than top-up amount, then does the deduction before adding employee
    //OR checks if top-up amount is <= 0, then no deduction and adds new employee 
    Corporate.check_and_update_corp_wallet(corp_id,topup, (result)=>{
        if(result.status==true){
            //add new employee
            Corporate.add_employee(pefum_code,coupon_code,first_name,last_name,phone,corp_id,corp_branch_id,corp_name,corp_branch,topup,wallet,fuel_station_subscribed,active,date,time,(response)=>{
                if(response.status==true){
                    //after adding employee, update user with coupon code
                    Corporate.update_user_coupon_n_wallet(pefum_code,coupon_code,wallet,(result)=>{
                        if(result.status==true){
                            res.status(200).json({
                                status:true,
                                message:response.message,
                                other:result.message,
                                response:response.response 
                            })
                        }else{
                            res.status(404).json({
                                status:false,
                                message:result.message
                            })
                        }
                    })
                }else{
                    res.status(404).json({
                        status:false,
                        message:response.message
                    })
                }
            })
        }else{
            res.status(500).json({
                status:false,
                message:result.message
            })
        }
    }) 
});


//GET ALL employees
router.get('/all_employees', verifyToken, (req, res)=>{
    var corp_id = req.UserID;

    //get all employees using corp_id
    Corporate.get_employees(corp_id,(response)=>{
        if(response.status==true){
            res.status(200).json({
                status:true,
                message:response.message,
                response:response.response 
            })
        }else{
            res.status(404).json({
                status:false,
                message:response.message
            })
        }
    })
});


//GET Branch employees
router.get('/branch_employees/:corp_branch_id', verifyToken, (req, res)=>{
    var corp_id = req.UserID;
    var corp_branch_id = req.params.corp_branch_id;

    //get all employees using corp_branch_id
    Corporate.get_branch_employees(corp_branch_id,(response)=>{
        if(response.status==true){
            res.status(200).json({
                status:true,
                message:response.message,
                response:response.response 
            })
        }else{
            res.status(404).json({
                status:false,
                message:response.message
            })
        }
    })
});



//GET ONE employee
router.get('/single_employee/:employee_id', verifyToken, (req, res)=>{
    var corp_id = req.UserID;
    var employee_id = req.params.employee_id;

    //get single employees using id
    Corporate.get_single_employee(employee_id,(response)=>{
        if(response.status==true){
            res.status(200).json({
                status:true,
                message:response.message,
                response:response.response 
            })
        }else{
            res.status(404).json({
                status:false,
                message:response.message
            })
        }
    })
});

//DELETE employee
router.post('/delete_employee', verifyToken, (req, res)=>{
    var corp_id = req.UserID;
    var employee_id = req.body.employee_id;

    //delete employee using employee code
    Corporate.delete_employee(employee_id,(response)=>{
        if(response.status==true){
            res.status(200).json({
                status:true,
                message:response.message,
                response:response.response 
            })
        }else{
            res.status(404).json({
                status:false,
                message:response.message
            })
        }
    })
    
});

//ACTIVATE/DEACTIVATE employee
router.get('/activate_deactivate_employee/:employee_id', verifyToken, (req, res)=>{
    var corp_id = req.UserID; //get station ID
    var employee_id = req.params.employee_id;

    Corporate.activate_deactivate_employee(employee_id,(response)=>{
        if(response.status==true){
            res.status(200).json({
                status:true,
                message:response.message
            })
        }else{
            res.status(404).json({
                status:false,
                message:response.message
            })
        }
    })
});


// TOP UP EMPLOYEE BALANCE
    router.post('/employee_top_up', verifyToken,(res,req)=>{
        var corp_id = req.UserID;
        var top_up = req.body.top_up;
        var employee_id = req.body.employee_id;

        var topup = parseFloat(top_up);

        //checks if corporate wallet balance is enough,then do deductions, then topup employee wallet
        Corporate.check_and_update_corp_wallet(corp_id,topup,(response)=>{
            if(response.status==true){
                //add topup to employee wallet and user coupon wallet
                Corporate.update_user_n_employee_wallet(employee_id,topup,(result)=>{
                    if(result.status==true){
                        res.status(200).json({
                            status:true,
                            message:result.message
                        })
                    }else{
                        res.status(500).json({
                            status:false,
                            message:result.message
                        })
                    }
                })
            }else{
                res.status(500).json({
                    status:false,
                    message:response.message
                })
            }
        })
    })

    
// TOP UP BRANCH BALANCE
router.post('/branch_top_up', verifyToken,(res,req)=>{
    var corp_id = req.UserID;
    var top_up = req.body.top_up;
    var corp_branch_id = req.body.corp_branch_id;

    var topup = parseFloat(top_up);

    //checks if corporate wallet balance is enough,then do deductions, then topup branch wallet
    Corporate.check_and_update_corp_wallet(corp_id,topup,(response)=>{
        if(response.status==true){
            //add topup to corporate branch wallet
            Corporate.update_branch_wallet(corp_branch_id,topup,(result)=>{
                if(result.status==true){
                    res.status(200).json({
                        status:true,
                        message:result.message,
                        other:response.message
                    })
                }
            })
        }else{
            res.status(500).json({
                status:false,
                message:response.message
            })
        }
    })
})



module.exports = router;