const express = require('express');
const bcrypt = require('bcrypt');
require('dotenv/config');
const jwt = require('jsonwebtoken');
const random = require('random');
const bodyparser = require('body-parser');
const verifyToken = require('../verifyToken');
const { route } = require('./attendant');

const router  = express.Router();


//Set up middlewares
router.use(bodyparser.json());
router.use(bodyparser.urlencoded({extended: false}));


router.post('/omc_login', async (req, res)=>{
    var Login_name = req.body.login_name;
    var Password = req.body.password;

    // get all omcs from omcDB
    var omcDB = [
        {
            login_name: "total001" ,
            omcid: 'omc001',
            password: "$2b$10$z6X/lnKXjyMcb2xojWU63uqdkE5wF64q1w4icpfQ.zi0GUPXGviBa"
        },
        {
            login_name: "shell001" ,
            omcid: 'omc002',
            password: "$2b$10$z6X/lnKXjyMcb2xojWU63uqdkE5wF64q1w4icpfQ.zi0GUPXGviBa"
        },
        {
            login_name: "goil001" ,
            omcid: 'omc003',
            password: "$2b$10$z6X/lnKXjyMcb2xojWU63uqdkE5wF64q1w4icpfQ.zi0GUPXGviBa"
            }, 
      
       ]


    //check if login name exist in omcDB
    var valid_login_name = validate_login_name(Login_name, omcDB); //returns {auth: bool, hashedPassword, omcid, omc_name}
     //check hash password with login password :Validity
     var valid_password = await bcrypt.compare(Password, valid_login_name.hashedPassword).then((result)=>{
            console.log('message: ', result)
        return result;
     }).catch((err)=>{
          console.log('custom error: ', err.message);
          return false;
        })

   
    if(valid_login_name.auth && valid_password){
      
        // generate token here with OMC id 
        var token = jwt.sign({UserID: valid_login_name.omcid }, process.env.SECRET_KEY);
        
        var id = valid_login_name.omcid;
        // console.log('Token', token)

        //sending response
        res.status(201).send({
            auth: true, 
            id,
            token: token,
            message: 'login successful'
        })  
        return;
       
    }

     //sending response
     res.status(404).send({
        auth: false, 
        message: 'incorrect login info'
    })
});


//USED TO AUTHENTICATE MOBILE NUMBER IN DATABASE
function validate_login_name(Login_name,omcDB){

    //check if mobile number exist in omcDB
    for(var i=0; i < omcDB.length; i++){
       // console.log('All', omcDB[i].mobile_phone)
       if(Login_name == omcDB[i].login_name){
           // console.log('Mobile', omcDB[i].mobile_phone)
           return {
               auth:true,
               login_name: Login_name,
               omcid: omcDB[i].omcid,
               hashedPassword: omcDB[i].password
            };
       }
   }
   return {
           auth: false,  
           login_name: null,
           omcid: null,
           hashedPassword: null 
       };
}


//CREATE NEW BRANCH
router.post('/create_branch', verifyToken, (req, res)=>{
    var omcid = req.UserID; //get station ID
    var branch_location = req.body.branch_location;
    var login_name = req.body.login_name;
    var password = req.body.password;

    try {
         //write code to check if branchid or/and omcid exit in OmcBranchDB before adding to DB eg. checker(col, tb) | checker(col, col, tb)
       
        //add new branch using omcid
        var data = {
            branchid: omcid + '.' + '001',
            omcid,
            branch_location,
            login_name,
            password
        }
        res.status(200).json({ 
            status: true,
            message: 'Branch created successfully',
            data})

    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error creating branch'
        })
    }
});



//GET ALL BRANCH
router.get('/all_branches',verifyToken, (req, res)=>{
    var omcid = req.UserID; //get station ID

    //get all branches under OMC station using omcid
    try {
        var branches = [
            {
                branchid: omcid + '.' + '001',
                omcid,
                branch_location: 'Spintex',
                
            },
            {
                branchid: omcid + '.' + '002',
                omcid,
                branch_location: 'Achimota',
            },
            {
                branchid: omcid + '.' + '003',
                omcid,
                branch_location: 'Accra',
            }, 
           ]

    res.status(200).json({ 
            status: true,
            message: 'Branch created successfully',
            OMC_Name: 'Goil',
            branches})

    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error creating branch'
        })
    }
});

//GET ONE BRANCH
router.get('/single_branch/:id', verifyToken, (req, res)=>{
    var omcid = req.UserID; //get station ID
    var branchID = req.params.id;

    try {
        //get branch using branchID
        var branch = {
            branchID,
            branch_location: 'Spintex',
            more: '...'
        }

   res.status(200).json({ 
            status: true,
            message: 'Branch  successfully',
            OMC_Name: 'Goil',
            branch})

    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error  branch'
        })
    }
});


//DELETE ONE BRANCH\
router.get('/delete_branch/:id', verifyToken, (req, res)=>{
    var omcid = req.UserID; //get station ID
    var branchID = req.params.id;

    try {
         //write code to check if branchid and omcid exit in OmcBranchDB before adding to DB eg. checker(col, tb) | checker(col, col, tb)
       
        //delete branch using branchID
        var branch = {
            branchID,
            branch_location: 'Spintex',
            more: '...'
        }

   res.status(200).json({ 
            status: true,
            message: 'Branch deleted successfully',
            OMC_Name: 'Goil',
            branch})

    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error deleting branch'
        })
    }
});



//ACTIVATE/DEACTIVATE BRANCH 
router.get('/activate_deactivate_branch/:id', verifyToken, (req, res)=>{
    var omcid = req.UserID; //get station ID
    var branchID = req.params.id;

    try {
         //write code to check if branchid and omcid exit in OmcBranchDB before adding to DB eg. checker(col, tb) | checker(col, col, tb)
       
        //get current branch activate_deactivate state and do this !state 
        var state = true; //active

        //activate or deactivate branch using branchID
        //update the active_deactive state
        var branch = {
            branchID,
            branch_location: 'Spintex',
            active_deactive: !state,
            more: '...'
        }

   res.status(200).json({ 
            status: true,
            message: 'Branch deactivated successfully',
            OMC_Name: 'Goil',
            branch})

    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error deactivating branch'
        })
    }
});



//GET  ALL TRANSACTIONS
router.get('/all_transaction', verifyToken, (req, res)=>{
    var omcid = req.UserID;

    //get all transactions using omcid
    try {
        // getting data here
        var transactions = [{
            id: '001',
            userid: '202',
            transaction_id: '00002222',
            attendant_code: '0394',
            omcid: omcid,
            fuel_station_id: 'omc1.001',
            code_type: 'P',
            pefum_corporate_code: '4455',
            amount: '300',
            fuel_type: 'Petrol',
            status: '0',
            date: '01-20-2020',
            time: '12:03am'
        }]
        
        res.status(200).json({ 
        status: true,
        message: 'all transactions fetched successfully',
        OMC_Name: 'Goil',
        transactions})

    } catch (error) {
    res.status(500).json({
        status: false,
        message: 'Error fetching all transactions'
    })
}
});



//GET  ATTENDANT TRANSACTIONS 
router.get('/attendant_transactions/:attendant_code', verifyToken, (req, res)=>{
    var omcid = req.UserID;
    var attendant_code = req.params.attendant_code;

    //get all transactions using omcid and attendant_code
    try {
        // getting data here
        var transactions = [{
            id: '001',
            userid: '202',
            transaction_id: '00002222',
            attendant_code,
            omcid: 'omc1',
            fuel_station_id: 'omc1' +'.'+ branchid,
            code_type: 'P',
            pefum_corporate_code: '4455',
            amount: '300',
            fuel_type: 'Petrol',
            status: '0',
            date: '01-20-2020',
            time: '12:03am'
        }]
        
        res.status(200).json({ 
        status: true,
        message: 'all transactions fetched successfully',
        OMC_Name: 'Goil',
        transactions})

    } catch (error) {
    res.status(500).json({
        status: false,
        message: 'Error fetching all transactions'
    })
}
});



//GET  BRANCH TRANSACTIONS 
router.get('/branch_transactions/:branchid', verifyToken, (req, res)=>{
    var omcid = req.UserID;
    var branchid = req.params.branchid;

    //get all transactions using omcid and branchid
    try {
        // getting data here
        var transactions = [{
            id: '001',
            userid: '202',
            transaction_id: '00002222',
            attendant_code: '1021',
            omcid,
            fuel_station_id: branchid,
            code_type: 'P',
            pefum_corporate_code: '4455',
            amount: '300',
            fuel_type: 'Petrol',
            status: '0',
            date: '01-20-2020',
            time: '12:03am'
        }]
        
        res.status(200).json({ 
        status: true,
        message: 'all transactions fetched successfully',
        OMC_Name: 'Goil',
        transactions})

    } catch (error) {
    res.status(500).json({
        status: false,
        message: 'Error fetching all transactions'
    })
}
});



//GET ONE TRANSACTION
router.get('/single_transaction/:transaction_id', verifyToken, (req, res)=>{
    var omcid = req.UserID;
    var transaction_id = req.params.transaction_id;

    //get transaction using transaction id and omcid
    try {
        // getting data here
        var transaction = {
            id: '001',
            userid: '202',
            transaction_id: transaction_id,
            attendant_code: '0394',
            omcid: omcid,
            fuel_station_id: 'omc1.001',
            code_type: 'P',
            pefum_corporate_code: '4455',
            amount: '300',
            fuel_type: 'Petrol',
            status: '0',
            date: '01-20-2020',
            time: '12:03am'
        }
        
        res.status(200).json({ 
            status: true,
            message: 'single transaction fetched successfully',
            OMC_Name: 'Goil',
            transaction})
        
    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error fetching single transactions'
        })
    }
});


//CREATE ATTENDANT
router.post('/create_attendant', verifyToken, async (req, res)=>{
    var omcid = req.UserID;
    var branchid = req.body.branch_id;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;
    var password = req.body.password;
    var mobile_number = req.body.mobile_number;
    var omc = req.body.omc;
    var branch = req.body.branch;
    var active_deactive = req.body.active_deactive;

    //create a attendant code
    var attendant_code = random.int(10000,1000000)

     //Hashing the password
     const hashedPassword = bcrypt.hashSync(password, 10);

    //create attendant & add to attendant db
    try {
         //write code to check if branchid and omcid exit in OmcBranchDB before adding to DB eg. checker(col, tb) | checker(col, col, tb)
        var data = { 
            omcid,
            branchid,
            attendant_code,
            first_name,
            last_name,
            mobile_number,
            password: hashedPassword,
            omc,
            branch, 
            active_deactive, 
            date: '01-20-2020',
            time: '12:03am'
        }
        res.status(200).json({ 
            status: true,
            message: 'attendant created successfully',
            data})
        
    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error creating attendant'
        })
    }
});


//GET ALL ATTENDANT
router.get('/all_attendants', verifyToken, (req, res)=>{
    var omcid = req.UserID;

    //get all attendants using omcid
    try {
        var data = [{ 
            omcid,
            branchid: omcid + '.' + '002',
            attendant_code: '20323',
            first_name: 'Kwame',
            last_name: 'Ofosu',
            mobile_number: '02132323',
            password: "$2b$10$Nq6/AwyFrrIizoDsUJlGcupSXO6gjOqEElEi/qg1YJc9dfG1lSVUC",
            omc: 'Shell',
            branch: 'Spintex', 
            active_deactive: true, 
            date: '01-20-2020',
            time: '12:03am'
        },
        { 
            omcid,
            branchid: omcid + '.' + '005',
            attendant_code: '34223',
            first_name: 'Kwaku',
            last_name: 'Arnold',
            mobile_number: '02132323',
            password: "$2b$10$Nq6/AwyFrrIizoDsUJlGcupSXO6gjOqEElEi/qg1YJc9dfG1lSVUC",
            omc: 'Shell',
            branch: 'Achimota', 
            active_deactive: true, 
            date: '01-20-2020',
            time: '12:03am'
        }]

        res.status(200).json({ 
            status: true,
            message: 'attendants list request successfully',
            data})
        
    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error getting all attendants'
        })
    }
});


//GET ALL ATTENDANT
router.get('/branch_attendants/:branchid', verifyToken, (req, res)=>{
    var omcid = req.UserID;
    var branchid = req.params.branchid;

    //get all attendants using omcid and branchid
    try {
        var data = [{ 
            omcid,
            branchid,
            attendant_code: '20323',
            first_name: 'Kwame',
            last_name: 'Ofosu',
            mobile_number: '02132323',
            password: "$2b$10$Nq6/AwyFrrIizoDsUJlGcupSXO6gjOqEElEi/qg1YJc9dfG1lSVUC",
            omc: 'Shell',
            branch: 'Spintex', 
            active_deactive: true, 
            date: '01-20-2020',
            time: '12:03am'
        },
        { 
            omcid,
            branchid: omcid + '.' + '005',
            attendant_code: '34223',
            first_name: 'Kwaku',
            last_name: 'Arnold',
            mobile_number: '02132323',
            password: "$2b$10$Nq6/AwyFrrIizoDsUJlGcupSXO6gjOqEElEi/qg1YJc9dfG1lSVUC",
            omc: 'Shell',
            branch: 'Achimota', 
            active_deactive: true, 
            date: '01-20-2020',
            time: '12:03am'
        }]

        res.status(200).json({ 
            status: true,
            message: 'attendants list request successfully',
            data})
        
    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error getting all attendants'
        })
    }
});



//GET ONE ATTENDANT
router.get('/single_attendant/:attendant_code', verifyToken, (req, res)=>{
    var omcid = req.UserID;
    var attendant_code = req.params.attendant_code;

    //get single attendants using attendant code
    try {
        var data ={ 
                    omcid,
                    branchid: omcid + '.' + '005',
                    attendant_code,
                    first_name: 'Kwaku',
                    last_name: 'Arnold',
                    mobile_number: '02132323',
                    password: "$2b$10$Nq6/AwyFrrIizoDsUJlGcupSXO6gjOqEElEi/qg1YJc9dfG1lSVUC",
                    omc: 'Shell',
                    branch: 'Achimota', 
                    active_deactive: true, 
                    date: '01-20-2020',
                    time: '12:03am'
                }

        res.status(200).json({ 
            status: true,
            message: 'attendant request successfully',
            data})
        
    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error getting attendant'
        })
    }
});

//DELETE ATTENDANT
router.post('/delete_attendant', verifyToken, (req, res)=>{
    var omcid = req.UserID;
    var attendant_code = req.body.attendant_code;

    //delete single attendants using omcid and attendant code
    try {
         //write code to check if branchid and omcid exit in OmcBranchDB before adding to DB eg. checker(col, tb) | checker(col, col, tb)
       
        var data ={ 
                    omcid,
                    branchid: omcid + '.' + '005',
                    attendant_code: attendant_code,
                    first_name: 'Kwaku',
                    last_name: 'Arnold',
                    mobile_number: '02132323',
                    password: "$2b$10$Nq6/AwyFrrIizoDsUJlGcupSXO6gjOqEElEi/qg1YJc9dfG1lSVUC",
                    omc: 'Shell',
                    branch: 'Achimota', 
                    active_deactive: true, 
                    date: '01-20-2020',
                    time: '12:03am'
                }

        res.status(200).json({ 
            status: true,
            message: 'attendant deleted successfully',
            data})
        
    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error deleting attendant'
        })
    }
});

//ACTIVATE/DEACTIVATE ATTENDANT
router.get('/activate_deactivate_attendant/:attendant_code', verifyToken, (req, res)=>{
    var omcid = req.UserID; //get station ID
    var attendant_code = req.params.attendant_code;

    try {
         //write code to check if branchid and omcid exit in OmcBranchDB before adding to DB eg. checker(col, tb) | checker(col, col, tb)
       
        //get current attendant activate_deactivate state and do this !state 
        var state = true; //active

        //activate or deactivate attendant using attendant code
        //update the active_deactive state
        var attendant = {
            attendant_code,
            branch: 'Spintex',
            active_deactive: !state,
            more: '...'
        }

   res.status(200).json({ 
            status: true,
            message: 'attendant deactivated successfully',
            OMC_Name: 'Goil',
            attendant})

    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error deactivating attendant'
        })
    }
});




module.exports = router;