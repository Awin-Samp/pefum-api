const express = require('express');
const bcrypt = require('bcrypt');
require('dotenv/config');
const jwt = require('jsonwebtoken');
const random = require('random');
const bodyparser = require('body-parser');
const verifyToken = require('../verifyToken');

const router  = express.Router();
//Attendant_db class
const attendant_db = require('../models/attendant_db.js');

// instatiate Attendant_db
let Attendant = new attendant_db();

router.use(bodyparser.json());
router.use(bodyparser.urlencoded({extended:true}));

//ATENDANT LOGIN ROUTE
router.post('/login', async (req, res)=>{
    var phone = Math.abs(req.body.phone);
    var password = req.body.password;

    //make sure parameters are not empty
    if(!phone || phone.length==0 || !password || password.length==0){
        console.log('ILOG', "empty parameters")
        res.status(404).send({
            status: false, 
            message: 'Enter all details!'
        })
        return;
    }

    // get attendant from attendantDB
        Attendant.get_attendant(phone, (response)=>{
        //    console.log("Attendant Password:", response.response.password)
            if(response.status==true){
                console.log('passwords', password, response.response.password)
                bcrypt.compare(password, response.response.password).then((result)=>{
                        // generate token here with UserID 
                            var token = jwt.sign({UserID: response.response.attendant_code }, process.env.SECRET_KEY);
                            // console.log('attendant token', token)
                            if(result ==true){
                                console.log('bcrypt message', result)
                                 //sending response 
                                return res.send({
                                    auth: true,
                                    token: token,
                                    message: 'Attendant login successful',
                                    user: response.response
                                }) 
                            }else{
                                res.status(404).send({
                                    status: false, 
                                    message: 'password incorrect!', 
                                })
                            }
                    }).catch((err)=>{ 
                        res.status(404).send({
                            auth: false, 
                            message: 'password error!'
                        })
                        return;
                        })
            }
            else{
                res.status(404).json({
                    status: false,
                    message: "phone number is invalid!",
                    other:response.message
                }) 
            } 
    });
});



//ATTENDANT ROUTES
router.get('/attendant', verifyToken, (req, res)=>{
    var attendant_code = req.attendant_code;

    //get attendant data from attendantDB using attendant_code
    try {
        var data = {
            id: '0010',
            attendant_code,
            first_name: 'John',
            last_name: 'Mensah',
            mobile_number: '547785025',
            password: "$2b$10$z6X/lnKXjyMcb2xojWU63uqdkE5wF64q1w4icpfQ.zi0GUPXGviBa",
            fuel_station: 'Shell',
            station_branch: 'Spintex',
            fuel_station_id: 'omc1.001',
            active_deactive: 'active', 
            date: '01-20-2020',
            time: '12:03am'
        }
        res.status(200).json({ status: true ,data})

    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error loading attendant data'
        })
    }

});



//TRANSACTION ROUTE
router.get('/transactions', verifyToken, (req, res)=>{
    var attendant_code = req.attendant_code; 
    //make sure parameters are not empty 
    if(!attendant_code || attendant_code.length==0){
        console.log('ILOG', "empty parameters")
        return res.status(404).send({
            status: false, 
            message: 'Enter all details!'
        })
    }
    //Get all transactions using attendant_code
    Attendant.get_transaction(attendant_code, (response)=>{
        if(response.status==true){
            res.status(200).json({ 
                status: true,
                message:response.message,
                response:response.response,
            })
            return;
        }
        else{
            res.status(500).json({
                status: false,
                message: 'Error loading transaction data',
                other:response.message
            })
            return;
        }
    })
});


//CREATE TRANSACTION ROUTE
router.post('/create_transaction', verifyToken, (req, res)=>{
    var code_type = req.body.code_type;
    var attendant_code = req.attendant_code;
    var fuel_station_id = req.body.fuel_station_id;
    var station_branch_id = req.body.station_branch_id;
    var amount = req.body.amount;
    var fuel_type = req.body.fuel_type;
    var status = req.body.status;
    // date and time
    const oldDate = new Date()
    var date = oldDate.toISOString().split('T')[0];
    var time  = new Date().toLocaleTimeString();  

    //make sure parameters are not empty 
    if(!attendant_code || !code_type || !fuel_station_id || !station_branch_id || !amount || !fuel_type || !status){
        console.log('ILOG', "empty parameters")
        return res.status(404).send({
            status: false, 
            message: 'Enter all details!'
        })
    }
    //Create unique transaction_id
    var transaction_id = random.int(1000000,100000000)
    console.log('transaction generated', transaction_id)
    //check code type and post to pefum transaction 
    if(code_type == 'pefum_code'){
        var pefum_code = req.body.pefum_code;
        //create pefum transaction
        Attendant.pefum_transaction(attendant_code,pefum_code,fuel_station_id,station_branch_id,amount,fuel_type,transaction_id,status,date,time,(response)=>{
            if(response.status !=false){
                Attendant.pefum_trigger(pefum_code, (result)=>{
                    if(result.status==true){
                        res.status(200).json({ 
                            status: true,
                            message: 'Transaction posted successfully(pefum)',
                            other:result.message
                            })
                    }else{
                        res.status(500).json({
                            status: false,
                            message: result.message
                        })
                    }   
                })
            }
            else{
                res.status(500).json({
                    status: false,
                    message: 'Error posting transaction data(pefum)',
                    other:response.message
                })
            }
        })
    }//check code type and post to coupon transaction
    else if(code_type =='coupon_code'){
        var coupon_code = req.body.coupon_code;
        //create coupon transaction
        Attendant.coupon_transaction(attendant_code,coupon_code,fuel_station_id,station_branch_id,amount,fuel_type,transaction_id,status,date,time,(response)=>{
            if(response.status!=false){
                Attendant.coupon_trigger(coupon_code, (result)=>{
                    if(result.status==true){
                        res.status(200).json({ 
                            status: true,
                            message: 'Transaction posted successfully(coupon)',
                            other:result.message
                            })
                    }else{
                        res.status(500).json({
                            status: false,
                            message: result.message
                        })
                    }  
                })
            }
            else{
                res.status(500).json({
                    status: false,
                    message: 'Error posting transaction data(coupon)'
                })
            }
        })
    }
    else{
        console.log('Error : Check code_type')
        res.status(500).json({
            status: false,
            message: 'Error : Check code_type'
        })
    }
});

// QRCODE REQUEST WITH PEFUM CODE
    router.get('/qrcode/:pefum_code',verifyToken,(req, res)=>{
        var pefum_code = req.params.pefum_code;
         //make sure parameters are not empty 
        if(!pefum_code || pefum_code.length==0){
            console.log('ILOG', "empty parameters")
            return res.status(404).send({
                status: false, 
                message: 'Enter all details!'
            })
        }
        //get basic user info
        Attendant.get_user_info(pefum_code, (response)=>{
            if(response.status==true){
                res.status(200).json({
                    status:true,
                    message:response.message,
                    response:response.response
                })
            }else{
                res.status(500).json({
                    status: false,
                    message:response.message
                })
            }
        })
    })

    // CANCEL TRANSACTION
    router.get('/cancel_transaction/:transaction_id', verifyToken, (req, res)=>{
        var transaction_id= req.params.transaction_id;

        // cancel transaction
        Attendant.cancel_transaction(transaction_id, (response)=>{
            if(response.status==true){
                res.status(200).json({
                    status:true,
                    message:response.message,
                    response:response.response
                })
            }else{
                res.status(500).json({
                    status: false,
                    message:response.message
                })
            }
        })
    })



module.exports = router;