const express = require('express');
const bcrypt = require('bcrypt');
require('dotenv/config');
const jwt = require('jsonwebtoken');
const random = require('random');
const bodyparser = require('body-parser');
const verifyToken = require('../verifyToken'); 

const router  = express.Router();


//Set up middlewares
router.use(bodyparser.json());
router.use(bodyparser.urlencoded({extended: false}));


router.post('/login', async (req, res)=>{
    var Login_name = req.body.login_name;
    var Password = req.body.password;

    // get all omcs from corpDB
    var corpDB = [
        {
            login_name: "ecobank201" ,
            corpid: 'corp001',
            branchid: 'corp002.001',
            branch:'Spintex',
            password: "$2b$10$z6X/lnKXjyMcb2xojWU63uqdkE5wF64q1w4icpfQ.zi0GUPXGviBa"
        },
        {
            login_name: "atlanticbank20" ,
            corpid: 'corp002',
            branchid: 'corp002.001',
            branch:'Spintex',
            password: "$2b$10$z6X/lnKXjyMcb2xojWU63uqdkE5wF64q1w4icpfQ.zi0GUPXGviBa"
        },
        {
            login_name: "Fidelitybank20" ,
            corpid: 'corp003',
            branchid: 'corp002.001',
            branch:'Spintex',
            password: "$2b$10$z6X/lnKXjyMcb2xojWU63uqdkE5wF64q1w4icpfQ.zi0GUPXGviBa"
            }, 
      
       ]


    //check if login name exist in corpDB
    var valid_login_name = validate_login_name(Login_name, corpDB); //returns {auth: bool, hashedPassword, branchid, Corp_Name}
     //check hash password with login password :Validity
     var valid_password = await bcrypt.compare(Password, valid_login_name.hashedPassword).then((result)=>{
            console.log('message: ', result)
        return result;
     }).catch((err)=>{
          console.log('custom error: ', err.message);
          return false;
        })

   
    if(valid_login_name.auth && valid_password){
      
        // generate token here with OMC id 
        var token = jwt.sign({UserID: valid_login_name.branchid }, process.env.SECRET_KEY);
        
        var id = valid_login_name.branchid;
        // console.log('Token', token)

        //sending response
        res.status(201).send({
            auth: true, 
            id,
            token: token,
            message: 'login successful'
        })  
        return;
       
    }

     //sending response
     res.status(404).send({
        auth: false, 
        message: 'incorrect login info'
    })
});


//USED TO AUTHENTICATE MOBILE NUMBER IN DATABASE
function validate_login_name(Login_name,corpDB){

    //check if mobile number exist in corpDB
    for(var i=0; i < corpDB.length; i++){
       // console.log('All', corpDB[i].mobile_phone)
       if(Login_name == corpDB[i].login_name){
           // console.log('Mobile', corpDB[i].mobile_phone)
           return {
               auth:true,
               login_name: Login_name,
               branchid: corpDB[i].branchid,
               hashedPassword: corpDB[i].password
            };
       }
   }
   return {
           auth: false,  
           login_name: null,
           branchid: null,
           hashedPassword: null 
       };
}



//GET ONE BRANCH
router.get('/', verifyToken, (req, res)=>{
    var branchid = req.UserID; //get station ID 

    try {
        //get branch using branchID
        var branch = {
            branchid,
            branch_location: 'Spintex',
            more: '...'
        }

   res.status(200).json({ 
            status: true,
            message: 'Branch  successfully',
            Corp_Name: 'Ecobank',
            branch})

    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error  branch'
        })
    }
});


//GET  ALL TRANSACTIONS
router.get('/all_transaction', verifyToken, (req, res)=>{
    var branchid = req.UserID;

    //get all transactions using branchid
    try {
        // getting data here
        var transactions = [{
            id: '001',
            userid: '202',
            transaction_id: '00002222',
            employee_id: '0394',
            omcid: 'omc003',
            branchid: branchid,
            fuel_station_id: 'omc1.001',
            code_type: 'P',
            pefum_corporate_code: '4455',
            amount: '300',
            fuel_type: 'Petrol',
            status: '0',
            date: '01-20-2020',
            time: '12:03am'
        }]
        
        res.status(200).json({ 
        status: true,
        message: 'all transactions fetched successfully',
        Corp_Name: 'Goil',
        transactions})

    } catch (error) {
    res.status(500).json({
        status: false,
        message: 'Error fetching all transactions'
    })
}
});



//GET  employee TRANSACTIONS 
router.get('/employee_transactions/:corporate_code', verifyToken, (req, res)=>{
    var branchid = req.UserID;
    var corporate_code = req.params.corporate_code;

    //get all transactions using branchid and corporate_code
    try {
        // getting data here
        var transactions = [{
            id: '001',
            userid: '202',
            transaction_id: '00002222',
            corporate_code,
            branchid,
            omcid: 'omc1',
            fuel_station_id: 'omc1.003',
            code_type: 'P',
            pefum_corporate_code: '4455',
            amount: '300',
            fuel_type: 'Petrol',
            status: '0',
            date: '01-20-2020',
            time: '12:03am'
        }]
        
        res.status(200).json({ 
        status: true,
        message: 'all transactions fetched successfully',
        Corp_Name: 'Ecobank',
        corporate_code,
        transactions})

    } catch (error) {
    res.status(500).json({
        status: false,
        message: 'Error fetching all transactions'
    })
}
});




//GET ONE TRANSACTION
router.get('/single_transaction/:transaction_id', verifyToken, (req, res)=>{
    var branchid = req.UserID;
    var transaction_id = req.params.transaction_id;

    //get transaction using transaction id and branchid
    try {
        // getting data here
        var transaction = {
            id: '001',
            userid: '202',
            transaction_id: transaction_id,
            employee_id: '0394',
            branchid: branchid,
            omcid: 'omc1',
            fuel_station_id: 'omc1.001',
            code_type: 'P',
            pefum_corporate_code: '4455',
            amount: '300',
            fuel_type: 'Petrol',
            status: '0',
            date: '01-20-2020',
            time: '12:03am'
        }
        
        res.status(200).json({ 
            status: true,
            message: 'single transaction fetched successfully',
            Corp_Name: 'Ecobank',
            transaction})
        
    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error fetching single transactions'
        })
    }
});


//CREATE employee
router.post('/create_employee', verifyToken, async (req, res)=>{
    var branchid = req.UserID;
    var corp_name = req.body.corp_name;
    var corpid = req.body.corpid;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name; 
    var pefum_code = req.body.pefum_code; 
    var amount = req.body.amount;
    var omc_subscribed = req.body.omc_subscribed; 
    var active_deactive = req.body.active_deactive;

    //create a corporate code
    var corporate_code = random.int(10000,1000000)

    //create employee & add to employee db
    try {
         //write code to check if branchid and branchid exit in corpBranchDB/corpDB before adding to DB eg. checker(col, tb) | checker(col, col, tb)

         //Also check if employee pefum code is in userdb before adding to employee db
         //if good, update userdb with corporate code and add new employee.
        var data = { 
           corpid,
           corp_name,
           branchid,
           first_name,
           last_name,
           pefum_code,
           corporate_code,
           amount,
           omc_subscribed,
            active_deactive, 
            date: '01-20-2020',
            time: '12:03am'
        }
        res.status(200).json({ 
            status: true,
            message: 'employee created successfully',
            data})
        
    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error creating employee'
        })
    }
});


//GET ALL employee
router.get('/all_employees', verifyToken, (req, res)=>{
    var branchid = req.UserID;

    //get all employees using branchid
    try {
        var data = [{ 
            branchid,
            "corp_name": "Ecobank",
            "first_name": "Kwame",
            "last_name": "Ofosu",
            "branchid": "corp.100",
            "pefum_code": "0343",
            "corporate_code": 498654,
            "amount": "2000",
            "omc_subscribed": "Goil",
            "active_deactive": "true",
            "date": "01-20-2020",
            "time": "12:03am"
        },
        { 
            branchid,
            "corp_name": "Ecobank",
            "first_name": "John",
            "last_name": "Manu",
            "branchid": "corp.100",
            "pefum_code": "0343",
            "corporate_code": 498654,
            "amount": "2000",
            "omc_subscribed": "Goil",
            "active_deactive": "true",
            "date": "01-20-2020",
            "time": "12:03am"
        }]

        res.status(200).json({ 
            status: true,
            message: 'employees list request successfully',
            data})
        
    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error getting all employees'
        })
    }
});


//GET ONE employee
router.get('/single_employee/:id', verifyToken, (req, res)=>{
    var branchid = req.UserID;
    var id = req.params.id;

    //get single employees using id
    try {
        var data ={ 
            id,
            branchid,
            branchid: 'corp001.322',
            "corp_name": "Ecobank",
            "first_name": "John",
            "last_name": "Manu", 
            "pefum_code": "0343",
            "corporate_code": 498654,
            "amount": "2000",
            "omc_subscribed": "Goil",
            "active_deactive": "true",
            "date": "01-20-2020",
            "time": "12:03am"
                }

        res.status(200).json({ 
            status: true,
            message: 'employee request successfully',
            data})
        
    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error getting employee'
        })
    }
});

//DELETE employee
router.post('/delete_employee', verifyToken, (req, res)=>{
    var branchid = req.UserID;
    var employee_id = req.body.employee_id;

    //delete single employees using branchid and employee code
    try {
         //write code to check if branchid and branchid exit in corpBranchDB before adding to DB eg. checker(col, tb) | checker(col, col, tb)
       
        var data ={ 
                    employee_id,
                    branchid,
                    branchid: 'corp001.322',
                    "corp_name": "Ecobank",
                    "first_name": "John",
                    "last_name": "Manu", 
                    "pefum_code": "0343",
                    "corporate_code": 498654,
                    "amount": "2000",
                    "omc_subscribed": "Goil",
                    "active_deactive": "true",
                    "date": "01-20-2020",
                    "time": "12:03am"
                }

        res.status(200).json({ 
            status: true,
            message: 'employee deleted successfully',
            data})
        
    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error deleting employee'
        })
    }
});

//ACTIVATE/DEACTIVATE employee
router.get('/activate_deactivate_employee/:employee_id', verifyToken, (req, res)=>{
    var branchid = req.UserID; //get station ID
    var employee_id = req.params.employee_id;

    try {
         
        //get current employee activate_deactivate state and do this !state 
        var state = true; //active

        //activate or deactivate employee using employee code
        //update the active_deactive state
        var employee = {
            employee_id,
            branch: 'Spintex',
            active_deactive: !state,
            more: '...'
        }

   res.status(200).json({ 
            status: true,
            message: 'employee deactivated successfully',
            Corp_Name: 'Ecobank',
            employee})

    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error deactivating employee'
        })
    }
});




module.exports = router;