const express = require('express');
const bodyparser = require('body-parser');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken'); 
const random = require('random'); 
const axios = require('axios');
const messagebird = require('messagebird')('jVq8ogQQ0OLwnaN0kJfWOdQRd');
require('dotenv/config');
const verifyToken = require('../verifyToken');
const router = express.Router();


//Set up middlewares
router.use(bodyparser.json());
router.use(bodyparser.urlencoded({extended: false}));

//client_db class
const client_db = require('../models/client_db.js');  

// instatiate client_db
let Client = new client_db();

const api_key='dEtPSmtscE53V1BrTmx4ck5pQng';
 

//REGISTRATION ROUTE
router.post('/register', (req,res)=>{
    // get values in request
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;
    var email = req.body.email;
    var phone = Math.abs(req.body.phone);
    var password = req.body.password;

    // date and time
    const oldDate = new Date()
    var date = oldDate.toISOString().split('T')[0];
    var time  = new Date().toLocaleTimeString();  

    //Hashing the password
    const hashedpassword = bcrypt.hashSync(password, 10);
    //create a PeFUM code
    var PeFUM_code = random.int(10000,100000)
    //create token(userID) for making request later
    var token = jwt.sign({UserID: PeFUM_code }, process.env.SECRET_KEY);
        console.log('Token', token)
    //generate OTP code 
    var OTP = random.int(10000,100000);
    console.log('OTP', OTP)
    //Convert mobile number to +2330547785025
    recipient_phone_number = '+233' + String(phone)
    console.log('custom number: ',recipient_phone_number)


    // Register new user upon sending SMS otp code   
    Client.register_user(first_name,last_name,email,phone,hashedpassword,PeFUM_code,OTP,true,date,time,
        (resp)=>{
            if(resp.status ==true){
                console.log('Registered user is OTP Verified',);
                res.status(201).send({
                    status:true,
                    message: 'New user created',
                    other: resp.message,
                    token: token,  
                });
                return;
            }else{
                res.status(201).send({
                    status: false,
                    message: 'Unsuccessful registration',
                    other: resp.message
                });
                return;
            }
        })
});

// RESEND OTP VIA SMS
router.post('/resend_otp', (req, res)=>{
    var phone = Math.abs(req.body.phone);
    
     //make sure parameters are not empty
     if(phone.length==0 || !phone){
        console.log('ILOG', "empty parameters")
        res.status(404).send({
            status: false, 
            message: 'Enter all  details!'
        })
        return;
    }

    //Convert mobile number to +2330547785025
    var recipient_phone_number = '+233' + String(phone)
    console.log('custom number: ',recipient_phone_number)

    // get OTP from database and send it via SMS
    Client.get_otp_by_phone(phone, (result)=>{
        if(result.status==true){
            console.info('get otp', result.response.otp_code)
             //send OTP code through SMS----------------------------------------------------
            var message =  String(result.response.otp_code) +' is your PeFUM verification code.\nPlease do NOT share this code.';
            var to = recipient_phone_number;
            var sender = 'PeFUM Inc';
            
            axios.get('https://sms.arkesel.com/sms/api?action=send-sms&api_key='+api_key+'& to='+to+'&from='+sender+'&sms='+message)
            .then(function (response) {
                console.log('RESPONSE:',response.data.code);
                if(response.data.code=='ok'){
                    res.status(500).send({
                        status: true,
                        message: 'OTP verification code sent (phone)', 
                    });    
                }else{
                    res.status(500).send({
                        status: false,
                        message: 'Error sending SMS OTP code',
                    });
                }
            })
            .catch(function (error) {
                console.log(error);
                res.status(500).send({
                    status: false,
                    message: 'kError sending SMS OTP code',
                });
            })
            //   ----------------------------------------
        }
        else{
            res.status(500).json({
                status: false,
                message:result.message
            })
        }
    })
});

// OTP VERIFY 
router.post('/verify_otp', verifyToken, (req, res)=>{
    var pefum_code = req.UserID;
    var otp_code = req.body.otp;
    console.log(pefum_code)

    //make sure parameters are not empty
    if(!otp_code || otp_code.length==0){
        console.log('ILOG', "empty parameters")
        res.status(404).send({
            status: false, 
            message: 'Enter all details!'
        })
        return;
    }

    // using the token-pefumcode, verify the otpcode entered
    Client.check_otp_by_pefum(pefum_code, otp_code,(response)=>{
        if(response.status==true){
            console.log('otp verified', response) 
            Client.phone_verified(pefum_code, (resp)=>{
               if(resp.status==true){
                    res.status(200).json({
                        status: true,
                        message: "OTP code verification successfull",
                        other:resp.message
                    });
               }else{
                res.status(404).json({
                    status: false,
                    message: "kkOTP code verification is successful",
                    other:resp.message
                });
            }
            })
        }
        else{
            res.status(404).json({
                status: false,
                message: "failed OTP code verification",
                other:response.message
            });
        }
    });

});

// LOGIN ROUTE
router.post('/login', async (req, res)=>{
    var phone = Math.abs(req.body.phone);
    var password = req.body.password;

    //CHECK Phone and Password if empty
    if(phone.length==0 || password.length==0 ||!phone || !password){
        console.log('ILOG', "empty parameters")
        res.status(404).send({
            status: false, 
            message: 'Enter all login details!'
        })
        return;
    }


       Client.get_user(phone, (response)=>{
        //    console.log("Client Password:", response  )
            if(response.status==true){
                // console.log("Client Password:", response.response.password)
                bcrypt.compare(password, response.response.password).then((result)=>{
            // generate token here with UserID 
                var token = jwt.sign({UserID: response.response.pefum_code }, process.env.SECRET_KEY);
                if(result ==true){
                    console.log('bcrypt message', result)
                     //sending response 
                    return res.send({
                        status: true,
                        token: token,
                        message: 'user login successful',
                        other:response.message,
                        user: response.response
                    }) 
                }else{
                    res.status(404).send({
                        status: false, 
                        message: 'password incorrect!', 
                    })
                }
           }).catch((err)=>{ 
               res.status(404).send({
                status: false, 
                message: 'password error!',
                other:response.message, 
            })
            return;
           })
        }
        else{
            res.status(404).json({
                status: false,
                message: "phone number can't be empty!",
                other:response.message
            }) 
        } 
    });
     
});

// UPDATE PROFILE INFO
router.post('/update_profile',verifyToken, (req, res)=>{
    var pefum_code = req.UserID;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;
    console.log('pefum code', pefum_code)

     //make sure parameters are not empty
     if(!first_name || last_name.length==0 || first_name.length==0 || !last_name){
        console.log('ILOG', "empty parameters")
        res.status(404).send({
            status: false, 
            message: 'Enter all details!'
        })
        return;
    }
    //update profile
    Client.update_profile(first_name,last_name,pefum_code,
        (response)=>{
            //after successful update
            if (response.status==true){
                res.status(200).send({
                    status: true, 
                    message: 'Profile update successful!',
                    other:response.message
                })
                return;
            }
            else{
                res.status(404).send({
                    status: false, 
                    message: 'failed to update profile!',
                    other:response.message
                })
                return;
            }
        })
})


//USERS ROUTE
router.get('/user', verifyToken, (req, res)=>{
    //Get tokens(Pefum code) from the verifyToken
    var UserID = req.UserID;

    
    try {
        // Get user data from client_db using UserID
        var data = {
            id: '001',
            userid: UserID,
            pefum_code: '4455',
            first_name: 'Enoch',
            last_name: 'Nketia',
            email: 'example@yahoo.com',
            phone: '547785025',
            password: '$2b$10$O2wOOnk2UIB4BWfaalmUnenXtpD6.LPsYUOc5SguTWgZvYz5E5H1C',
            profile_pic: '',
            wallet: '3000',
            otp_code: 26255,
            phone_verified: true,
            email_verified: true,
            active_deactive: 'active',
            pending_status: 0,
            date: '01-20-2020',
            time: '12:03am'
        }

        res.status(200).json({ status: true ,data})

    } catch (error) {
        res.status(500).json({
            status: false,
            message: 'Error loading user data'
        })
    }
    console.log('UserID ', UserID)

    // res.send('user')
});


//TRANSACTION ROUTE
router.get('/transactions', verifyToken, (req, res)=>{
    var pefum_code = req.UserID;
        // Get transaction data using Pefum code
        Client.get_transaction(pefum_code, (response)=>{
            if(response.status==true){
                res.status(200).json({ 
                    status: true ,
                    response:response.response,
                    other:response.message
                })
                return;
            }
            else{
                res.status(500).json({
                    status: false,
                    message: 'Error loading transaction data',
                    other:response.message
                })
                return;
            }
        })
    });


// PENDING TRANSACTION
router.get('/pending_transaction', verifyToken, (req, res)=>{
    var pefum_code = req.UserID; 
     // Get pending transaction data using Pefum code
     Client.get_pending_transaction(pefum_code,(response)=>{
         
        if(response.status==true){
            res.status(200).json({ 
                status: true ,
                response:response.response,
                other:response.message
            })
        }
        else{
            res.status(500).json({
                status: false,
                message: 'Error loading user data',
                other:response.message
            })
            return;
        }
    })
})


//APPROVE TRANSACTIONS
router.post('/approve_transaction', verifyToken, (req, res)=>{
    var pefum_code = req.UserID;
    var transaction_id = req.body.transaction_id;
     //make sure parameters are not empty
     if(!transaction_id || transaction_id.length==0){
        console.log('ILOG', "empty parameters")
        res.status(404).send({
            status: false, 
            message: 'Enter all details!'
        })
        return;
    }
    // approve transaction
    Client.approve_transaction(transaction_id, pefum_code, (response)=>{
        if(response.status==true){
            //change user pending status to 0
            Client.update_user_pending_status(pefum_code, (result)=>{
                if(result.status==true){
                    res.status(200).json({
                        status:response.status,
                        transaction_id,
                        message:'Transaction approved',
                        other: result.message
                    })
                }else{
                    res.status(200).json({
                        status:false, 
                        message:'Failed: Transaction approved',
                        other: result.message
                    })
                }
            })
        }else{
            res.status(505).json({
                status:false, 
                message:'Failed: Transaction approved',
                other:response.message
            })
        }
    })
})

//DECLINE TRANSACTIONS
router.post('/decline_transaction', verifyToken, (req, res)=>{
    var pefum_code = req.UserID;
    var transaction_id = req.body.transaction_id;

     //make sure parameters are not empty
     if(!transaction_id || transaction_id.length==0){
        console.log('ILOG', "empty parameters")
        res.status(404).send({
            status: false, 
            message: 'Enter all details!'
        })
        return;
    }
    // approve transaction
    Client.decline_transaction(transaction_id, (response)=>{
        if(response.status !=false){
            //change user pending status to 0 or completed
            Client.update_user_pending_status(pefum_code, (result)=>{
                if(result.status!=false){
                    res.status(200).json({
                        status:response.status, 
                        message:response.message,
                        other: result.message
                    })
                }else{
                    res.status(500).json({
                        status:false, 
                        message:'Failed: Transaction decline',
                        other: result.message
                    })
                }
            })
        }else{
            res.status(500).json({
                status:false, 
                message:'Failed: Transaction decline',
                other: response.message
            })
        }
    })
})


//WALLET ROUTE
router.post('/wallet', verifyToken, (req, res)=>{
    var pefum_code = req.UserID; 
    
    //after payment made successfully using PAYSTACK  
    var amount= '4000';
    var payment_method='Mobile Money';
    var first_name='Awin';
    var last_name='Samp';
    var phone='';
    var card_details='0324239329393';
    //Create unique transaction_id
    var transaction_id = random.int(1000000,100000000)
    var message='';
    var status='1';
    // date and time
    const oldDate = new Date()
    var date = oldDate.toISOString().split('T')[0];
    var time  = new Date().toLocaleTimeString();  
     //Data from PAYSTACK
    //update deposite db with paystack data--------------------------------------------------
    Client.add_deposite(pefum_code,amount,payment_method,first_name,last_name,phone,card_details,transaction_id,message,status,date,time, (response)=>{
        
        if(response.status==true){
            //update userDB wallet with pefum_code
            //get current wallet balance
            console.log('deposit console', response)
            Client.get_wallet_balance(pefum_code, pefum_code, (wallet_balance)=>{
                var balance = parseFloat(wallet_balance.response.pefum_wallet);
                var new_balance = balance + parseFloat(amount);
                console.log('balance', balance)
                console.log('new balance', new_balance)
                // update wallet with new balance
                Client.update_wallet(pefum_code,pefum_code, new_balance, (response)=>{
                    if(response.status==true){
                        console.log('response', response)
                        res.status(200).json({
                            status:true,
                            message:response.message
                        });
                    }else{ 
                        res.status(500).json({
                            status:false,
                            message:response.message
                        });
                    }
                   
                })
            })
        }else{
            res.status(500).json({
                status: false,
                message: 'Error updating wallet data',
                other: response.message
            }) 
        }
    })
});

// COUPON WALLET HERE
    router.get('/coupon_wallet', verifyToken, (req,res)=>{
        var pefum_code = req.UserID;
        Client.get_coupon_balance(pefum_code, (response)=>{
            if(response.status==true){
                res.status(200).json({
                    status:true,
                    response:response.response,
                    message:response.message
                })
            }
            else{
                res.status(500).json({
                    status:false,
                    message:response.message
                })
            }
        })
    })

//  RESETING password STEPS

    // STEP 1) Send SMS new OTP and update db with new OTP 
    router.post('/forgot_password', (req, res)=>{
        var phone_number = Math.abs(req.body.phone);

         //make sure parameters are not empty
    if(!phone_number || phone_number.length==0){
        console.log('ILOG', "empty parameters")
        res.status(404).send({
            status: false, 
            message: 'Enter all details!'
        })
        return;
    }

        //Convert mobile number to +233547785025
        var recipient_phone_number = '+233' + String(phone_number)
    console.log('recipient', recipient_phone_number)        
        //generate OTP code 
        var OTP = random.int(10000,100000);

        // update OTP in usersDB client_db using phone number----------------------------
        Client.change_otp(phone_number, OTP, (response)=>{
            if(response.status==true){
                //send OTP code through SMS
                //send OTP code through SMS----------------------------------------------------
                var message =  String(OTP) +' is your PeFUM Password Reset verification code.\nPlease do NOT share this code.';
                var to = recipient_phone_number;
                var sender = 'PeFUM Inc';

                axios.get('https://sms.arkesel.com/sms/api?action=send-sms&api_key='+api_key+'& to='+to+'&from='+sender+'&sms='+message)
                .then(function (response) {
                    console.log('SMS RESPONSE:',response.data.code);
                    if(response.data.code=='ok'){
                        res.status(201).send({
                            status:true,
                            OTP,
                            message: 'Reset password processing,  OTP sent via SMS',  
                        }); 
                    }else{
                        res.status(500).send({
                            status: false,
                            message: 'Error sending SMS OTP code',
                        });
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    res.status(500).send({
                        status: false,
                        message: 'Error sending SMS OTP code',
                    });
                })
                //   ----------------------------------------
           }
            else{
                res.status(500).send({
                    status: false, 
                    other:response.message
                }); 
            }
        })
    });


    //STEP 2) CONFIRM USER OTP WITH userDB OTP 
    router.post('/reset_confirm', (req, res)=>{
        var user_otp = req.body.otp;
        var phone_number = Math.abs(req.body.phone);
         //make sure parameters are not empty
        if(!phone_number || phone_number.length==0 || !user_otp || user_otp.length==0){
            console.log('ILOG', "empty parameters")
            res.status(404).send({
                status: false, 
                message: 'Enter all details!'
            })
            return;
        }
            // check if user_otp is in userDB using phone number (547785025)
            Client.check_otp(phone_number, user_otp, (response)=>{
                if(response.status==true){
                    var pefum_code = response.pefum_code;
                    //create token(userID) for making request later
                    var token = jwt.sign({UserID: pefum_code }, process.env.SECRET_KEY);
                    res.status(201).json({
                        status: true, 
                        token,
                        message: ' Reset PERMISSION GRANTED'
                    })
                }else{
                    res.status(500).send({
                        status: false,
                        mobile_phone: phone_number,
                        message: ' Reset PERMISSION not granted', 
                    });
                }
            })
        });


    //STEP 3 RESET password
    router.post('/reset_password', verifyToken, (req, res)=>{
        var phone_number = Math.abs(req.body.phone);
        var password = req.body.password;
        var reset_password = req.body.reset_password;

         //make sure parameters are not empty
    if(!phone_number || phone_number.length==0 || !password || password.length==0){
        console.log('ILOG', "empty parameters")
        res.status(404).send({
            status: false, 
            message: 'Enter all details!'
        })
        return;
    }

        //check if both passwords match
        if(password == reset_password){
            //encrypt password 
            var hashedpassword =  bcrypt.hashSync(password, 10);
            console.log('password', hashedpassword)
            // update password in userDB using phone number or pefum_code
            Client.reset_password(phone_number,hashedpassword, (response)=>{
                if(response.status==true){
                    res.status(200).json({
                        status:true,
                        message:response.message
                    })
                }else{
                    res.status(501).send({
                        status: false,
                        message: 'Unsuccessful password reset',
                        other:response.message
                    })
                }
            })        
        }else{
            res.status(501).send({
                status: false,
                message: 'Unsuccessful password reset'
            })
        }
    });

module.exports = router;