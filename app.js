const express = require('express');
const bcrypt = require('bcrypt');
const bodyparser = require('body-parser'); 
const jwt = require('jsonwebtoken');
const mysql = require('mysql');
require('dotenv/config');
const verifyToken = require('./verifyToken.js');
const axios = require('axios');
const random = require('random'); 



// Getting all controllers
const clientRoute = require('./controllers/client.js');
const attendantRoute = require('./controllers/attendant');
const omcRoute = require('./controllers/omc');
const omcBranchRoute = require('./controllers/omc_branch');
const corporateRoute = require('./controllers/corporate');
const corporateBranchRoute = require('./controllers/corporate_branch'); 

//Instantiate express()
const app = express();

//Instantiate bodyparser for json and urlencoded
app.use(bodyparser.urlencoded({extended: false}));
app.use(bodyparser.json());

//set up port number
const PORT = process.env.PORT || 1000;


// Setup all controllers
app.use('/api/client',clientRoute);
app.use('/api/attendant',attendantRoute);
app.use('/api/omc',omcRoute);
app.use('/api/omc/branch', omcBranchRoute);
app.use('/api/corp', corporateRoute);
app.use('/api/corp/branch', corporateBranchRoute);

//default route
app.get('/', verifyToken, function(req, res, next){ 
    var token = req.headers['token'];
    // console.log('token decoded', req.PeFUM_code)

    res.send({"auth":"Permission granted"})
});

app.get('/sms', (req, res)=>{
    var otp = random.int(1000,10000);
    var api_key='dEtPSmtscE53V1BrTmx4ck5pQng';
    var message =  String(otp) +' is your PeFUM verification code.\nPlease do NOT share this code.';
    var to = '233560467846';
    var sender = 'PeFUM Inc';

    axios.get('https://sms.arkesel.com/sms/api?action=send-sms&api_key='+api_key+'& to='+to+'&from='+sender+'&sms='+message)
      .then(function (response) {
        console.log('RESPONSE:',response.data.code);
        if(response.data.code=='ok'){
            res.send({status:true, message:'sms sent'});
        }else{
            res.send({status:false, message:'error sending sms '});
        }
      })
      .catch(function (error) {
        console.log(error);
        res.send({status:false, message:'serious error sending sms '});
      })

//     axios.get('https://sms.arkesel.com/sms/api?action=send-sms&api_key=dEtPSmtscE53V1BrTmx4ck5pQng=&to=233560467846&from=PFM&sms=Hello world. Spreading peace and joy only. Remeber to put on your face mask. Stay safe!')
//     .then((response) => {
//         console.log(response)
//         res.send({status:true, message:'sms sent'});
//     })
//     .catch(error => console.log(error));
})



app.listen(PORT, ()=> console.log('Server running on ', PORT))